@extends('administrador.layouts.app-admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <a href="{{route('management.create')}}" class="-btn-agregar">
                    <i class="fas fa-user-plus"></i>
                    Agregar
                </a>
            </div>
        </div>
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('success') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        Destination Management
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover mt-3">
                            <thead>
                            <tr>
                                <th width="200px">Imagen</th>
                                <th>Liga</th>
                                <th colspan="3" width="50px">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($managements->sortby('name') as $management)
                                <tr>
                                    <td width="200px">
                                        <a href="{{route('management.edit', ['management' => $management->id])}}">
                                            @if($management->image)
                                                <img src="{{asset($management->image)}}" class="img-fluid" alt="{{$management->image}}" id="logo">
                                            @else
                                                <img src="{{asset('images/missing.png')}}" class="img-fluid" alt="Sin imagen" id="logo">
                                            @endif
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{route('management.edit', ['management' => $management->id])}}">
                                            {{$management->url}}
                                        </a>
                                    </td>
                                    <td width="10px">
                                        <form method="POST" action="{{ route('management.active', ['id' => $management->id]) }}">
                                            @csrf
                                            <button type="submit" class="btn">
                                                @if($management->active)
                                                    <i class="fa fa-power-off -on"></i>
                                                @else
                                                    <i class="fa fa-power-off -off"></i>
                                                @endif
                                            </button>
                                        </form>
                                    </td>
                                    <td width="10px">
                                        <form method="POST" action="{{ route('management.destroy', ['management' => $management->id]) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn text-danger">
                                                <i class="fa fa-trash-alt"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
