<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', 'KomexTours')</title>

    <!-- Scripts -->
    <script src="{{asset('js/jquery-3.5.1.min.js')}}"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('administrador/css/admin.css') }}" rel="stylesheet">
    @yield('styles')
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md  -topnav">
            <a class="navbar-brand" href="{{ route('index') }}">
                <img src="{{asset('images/excess.svg')}}" class="img-fluid" width="120px">
            </a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right -logout text-center" aria-labelledby="navbarDropdown">
                            <form action="{{ route('logout') }}" method="post">
                                @csrf
                                <button class="btn">
                                    <span>Cerrar sesión</span>
                                </button>
                            </form>
                        </div>
                    </li>

                </ul>
            </div>
        </nav>
        <div class="container-fluid">
            <div class="row">
                <div class="-sidebar -side">
                    <div class="-sidebar">
                        <nav id="sidebar">
                            <div class="barra-idioma text-center">
                                @if (App::isLocale('en'))
                                    <h6 class="subtitle text-white">
                                        Estas administrando en Idioma
                                        <i class="fa fa-flag text-info"></i>
                                        Inglés
                                    </h6>
                                @elseif (App::isLocale('es'))
                                    <h6 class="subtitle text-white">
                                        Estas administrando en Idioma
                                        <i class="fa fa-flag text-success"></i>
                                        Español
                                    </h6>
                                @endif
                            </div>
                            <div class="col-10 offset-1">

                                @if (App::isLocale('en'))
                                    <a class="btn btn-outline-success" href="{{ url('lang', ['es']) }}">
                                        Administrar en Español
                                    </a>
                                @elseif (App::isLocale('es'))

                                    <a class="btn btn-outline-info" href="{{ url('lang', ['en']) }}">
                                        Administrar en Inglés
                                    </a>
                                @endif
                            </div>
                            <div class="mt-5">
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="{{route('users.index')}}" class="{{ $title == 'Usuarios' ? 'active':'' }}" ><i class="fa fa-users"></i> Usuarios</a>
                                    </li>
                                    <li>
                                        <a href="{{route('fairs.index')}}" class="{{ $title == 'Ferias' ? 'active':'' }}" ><i class="fa fa-flag"></i> Ferias</a>
                                    </li>
                                    <li>
                                        <a href="{{route('extensions.index')}}" class="{{ $title == 'Extensiones' ? 'active':'' }}"><i class="fa fa-globe-europe"></i> Extensiones</a>
                                    </li>
                                    <li>
                                        <a href="{{route('mexico.index')}}" class="{{ $title == 'México' ? 'active':'' }}"><i class="fa fa-globe-americas"></i> México</a>
                                    </li>
                                    <li>
                                        <a href="{{route('trips.index')}}" class="{{ $title == 'Viajes Especiales' ? 'active':'' }}"><i class="fa fa-plane"></i> Viajes Especiales</a>
                                    </li>
                                    <li>
                                        <a href="{{route('offers.index')}}" class="{{ $title == 'Ofertas' ? 'active':'' }}"><i class="fa fa-comments-dollar"></i> Ofertas</a>
                                    </li>
                                    <li>
                                        <a href="{{route('leagues.index')}}" class="{{ $title == 'Ligas' ? 'active':'' }}"><i class="fa fa-link"></i> Ligas</a>
                                    </li>
                                    <li>
                                        <a href="{{route('promotions.index')}}" class="{{ $title == 'Promociones' ? 'active':'' }}"><i class="fa fa-star"></i> Promociones</a>
                                    </li>
                                    <li class="">
                                        <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-rss"></i>Comunicados</a>
                                        <ul class="collapse list-unstyled {{ $title == 'Noticias' ? 'show':'' }} {{ $title == 'Blog' ? 'show':'' }}" id="homeSubmenu">
                                            <li>
                                                <a href="{{route('news.index')}}" class="{{ $title == 'Noticias' ? 'active':'' }}"><i class="fa fa-newspaper"></i> Noticias</a>
                                            </li>
                                            <li>
                                                <a href="{{route('blogs.index')}}" class="{{ $title == 'Blog' ? 'active':'' }}"><i class="fa fa-blog"></i> Blog</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{route('management.index')}}" class="{{ $title == 'Destination Management' ? 'active':'' }}"><i class="fa fa-star"></i> Destination Management</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
                <div class="col -contenido">
                    <div class="container-fluid -barra-de-titulo">
                        <h3> {{ $title  }}
                    </div>
                    @yield('content')
                </div>

            </div>
        </div>
    </div>
    <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/libs/jquery-1.6.2.min.js"><\/script>')</script>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(window).ready(function (){
        var images = $(".image");

        $(images).on("error", function(event) {
            $(event.target).css("display", "none");
        });

    });
</script>
@yield('scripts')
</html>
