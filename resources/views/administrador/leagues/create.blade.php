@extends('administrador.layouts.app-admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1">
                <div class="row">
                    <div class="col-12 d-flex justify-content-between">
                        <h1>Crear Nueva Liga</h1>
                        <div class="col-2">
                            <a href="{{route('leagues.index')}}" class="btn btn-primary">
                                <i class="fa fa-list"></i>
                                Lista
                            </a>
                        </div>
                    </div>
                </div>
                <form class="validate-form" method="POST" action="{{route('leagues.store')}}">
                    @csrf
                    <label for="">
                        @if(App::islocale('es'))
                            <i class="fa fa-flag text-success"></i>
                        @endif
                        @if(App::islocale('en'))
                            <i class="fa fa-flag text-info"></i>
                        @endif
                        Nombre
                    </label>
                    <input type="text" class="form-control mt-3 @error('name') is-invalid @enderror" placeholder="Nombre" name="name" value="{{old('name')}}">
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    <label for="">Liga</label>
                    <input type="text" class="form-control mt-3 @error('league') is-invalid @enderror" placeholder="Url" name="league" value="{{old('league')}}">
                    @error('league')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    <div class="row mt-3">
                        <div class="col-3 offset-9">
                            <button type="submit" class="btn btn-block btn-primary">
                                <i class="fa fa-save"></i>
                                Guardar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
