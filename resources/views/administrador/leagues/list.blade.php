@extends('administrador.layouts.app-admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <a href="{{route('leagues.create')}}" class="-btn-agregar">
                    <i class="fas fa-user-plus"></i>
                    Agregar
                </a>
            </div>
        </div>
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('success') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Ligas
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover mt-3">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Liga</th>
                                <th colspan="2">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($leagues->sortby('name') as $league)
                                <tr>
                                    <td>{{$league->id}}</td>
                                    <td>
                                        @if($league->name)
                                            <a href="{{route('leagues.edit', ['league' => $league->id])}}">{{$league->name}}</a>
                                        @else
                                            <a href="{{route('leagues.edit', ['league' => $league->id])}}">Editar</a>
                                        @endif
                                    </td>
                                    <td>{{$league->url}}</td>
                                    <td width="10px">
                                        <form method="POST" action="{{ route('leagues.active', ['id' => $league->id]) }}">
                                            @csrf
                                            <button type="submit" class="btn">
                                                @if($league->active)
                                                    <i class="fa fa-power-off -on"></i>
                                                @else
                                                    <i class="fa fa-power-off -off"></i>
                                                @endif
                                            </button>
                                        </form>
                                    </td>
                                    <td width="10px">
                                        <form method="POST" action="{{ route('leagues.destroy', ['league' => $league->id]) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn text-danger">
                                                <i class="fa fa-trash-alt"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
