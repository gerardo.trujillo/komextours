@extends('administrador.layouts.app-admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1">
                <div class="row">
                    <div class="col-12 d-flex justify-content-between">
                        @if($title == 'Noticias')
                            <h1>Crear Nueva Noticia</h1>
                            <div class="col-2">
                                <a href="{{route('news.index')}}" class="btn btn-primary">
                                    <i class="fa fa-list"></i>
                                    Lista
                                </a>
                            </div>
                        @elseif($title == 'Blog')
                            <h1>Crear Nuevo Blog</h1>
                            <div class="col-2">
                                <a href="{{route('blogs.index')}}" class="btn btn-primary">
                                    <i class="fa fa-list"></i>
                                    Lista
                                </a>
                            </div>
                        @endif

                    </div>
                </div>
                @if($title == 'Noticias')
                    <form class="validate-form mb-5" method="POST" action="{{route('news.store')}}" enctype="multipart/form-data">
                @elseif($title == 'Blog')
                    <form class="validate-form mb-5" method="POST" action="{{route('blogs.store')}}" enctype="multipart/form-data">
                @endif
                    @csrf
                    <div class="row mt-5">
                        <div class="col-12">
                            <label for="">
                                @if(App::islocale('es'))
                                    <i class="fa fa-flag text-success"></i>
                                @endif
                                @if(App::islocale('en'))
                                    <i class="fa fa-flag text-info"></i>
                                @endif
                                Titulo
                            </label>
                                <input type="text" class="form-control mt-3 @error('title') is-invalid @enderror" placeholder="Titulo" name="title" value="{{old('title')}}">
                            @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-12">
                            <label for="">
                                @if(App::islocale('es'))
                                    <i class="fa fa-flag text-success"></i>
                                @endif
                                @if(App::islocale('en'))
                                    <i class="fa fa-flag text-info"></i>
                                @endif
                                Introducción
                            </label>
                            <textarea class="ckeditor mt-3 @error('intro') is-invalid @enderror" placeholder="Introducción"
                                      name="intro" value="{{old('intro')}}">{{old('intro')}}</textarea>
                            @error('intro')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-12">
                            <label for="">
                                @if(App::islocale('es'))
                                    <i class="fa fa-flag text-success"></i>
                                @endif
                                @if(App::islocale('en'))
                                    <i class="fa fa-flag text-info"></i>
                                @endif
                                Contenido
                            </label>
                            <textarea class="ckeditor mt-3 @error('content') is-invalid @enderror" placeholder="Contenido"
                                      name="content" value="{{old('content')}}">{{old('content')}}</textarea>
                            @error('content')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-4">
                            <label class="text-center">Imagen</label>
                            <br>
                            <img src="{{asset('images/missing.png')}}" class="img-fluid" alt="Sin imagen" id="image">
                        </div>
                        <div class="col-8">
                            <br><br>
                            <div class="alert alert-primary" role="alert">
                                <i class="fa fa-file-image"></i>
                                Imagen (Formato png, jpg o jpeg 230 x 360 resolución 72 dpi, max 900Kb )<br>
                            </div>

                            <div class="upload-btn-wrapper" id="btn">
                                <button class="btn btn-primary">Selecciona un archivo</button>
                                <input type="file" name="image" accept="image/*" onchange="loadFile(event)">
                            </div>
                            <div id="delete">
                                <a class="btn btn-danger" onclick="deletePoster()">Eliminar Imagen</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <input type="checkbox" class="checkbox" name="active" id="active" checked>
                            <label for="active">Activo</label>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-3 offset-9">
                            <button type="submit" class="btn btn-block btn-primary">
                                <i class="fa fa-save"></i>
                                Guardar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        document.getElementById('delete').style.display = 'none';
        function deletePoster(){
            document.getElementById('delete').style.display = 'none';
            document.getElementById('btn').style.display = 'block';
            var image = document.getElementById('image');
            image.src = '/images/missing.png';
            image.value = "";
        }
        var loadFile = function(event) {
            var reader = new FileReader();
            reader.onload = function(){
                var image = document.getElementById('image');
                image.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
            document.getElementById('btn').style.display = 'none';
            document.getElementById('delete').style.display = 'block';
        };
    </script>
@endsection
