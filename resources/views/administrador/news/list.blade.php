@extends('administrador.layouts.app-admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                @if($title == 'Noticias')
                    <a href="{{route('news.create')}}" class="-btn-agregar">
                @elseif($title == 'Blog')
                    <a href="{{route('blogs.create')}}" class="-btn-agregar">
                @endif
                    <i class="fas fa-user-plus"></i>
                    Agregar
                </a>
            </div>
        </div>
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('success') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        Comunicados
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover mt-3">
                            <thead>
                            <tr>
                                <th width="150px">Imagen</th>
                                <th>Nombre</th>
                                <th>Introducción</th>
                                <th>Fecha</th>
                                <th colspan="2">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($newsAll as $news)
                                <tr>
                                    <td width="200px">
                                        @if($title == 'Noticias')
                                            <a href="{{route('news.edit', ['news' => $news->id])}}">
                                        @elseif($title == 'Blog')
                                            <a href="{{route('blogs.edit', ['blog' => $news->id])}}">
                                        @endif
                                        @if($news->image)
                                            <img src="{{asset($news->image)}}" class="img-fluid" alt="{{$news->image}}" id="poster">
                                        @else
                                            <img src="{{asset('images/missing.png')}}" class="img-fluid" alt="Sin imagen" id="poster">
                                        @endif
                                        </a>
                                    </td>
                                    <td>
                                        @if($title == 'Noticias')
                                            <a href="{{route('news.edit', ['news' => $news->id])}}">
                                        @elseif($title == 'Blog')
                                            <a href="{{route('blogs.edit', ['blog' => $news->id])}}">
                                        @endif
                                            {{$news->title}}
                                        </a>
                                    </td>
                                    <td>{!! Str::words(trans($news->intro), 20, '...') !!}</td>
                                    <td width="150px">{{$news->date}}</td>
                                    <td width="10px">
                                        @if($title == 'Noticias')
                                            <form method="POST" action="{{ route('news.active', ['id' => $news->id]) }}">
                                        @elseif($title == 'Blog')
                                            <form method="POST" action="{{ route('blogs.active', ['id' => $news->id]) }}">
                                        @endif
                                            @csrf
                                            <button type="submit" class="btn">
                                                @if($news->active)
                                                    <i class="fa fa-power-off -on"></i>
                                                @else
                                                    <i class="fa fa-power-off -off"></i>
                                                @endif
                                            </button>
                                        </form>
                                    </td>
                                    <td width="10px">
                                        @if($title == 'Noticias')
                                            <form method="POST" action="{{ route('news.destroy', ['news' => $news->id]) }}">
                                        @elseif($title == 'Blog')
                                            <form method="POST" action="{{ route('blogs.destroy', ['blog' => $news->id]) }}">
                                        @endif
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn text-danger">
                                                <i class="fa fa-trash-alt"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
