@extends('administrador.layouts.app-admin')

@section('styles')
    <style type="text/css">
        #map {
            width:100%;
            height: 500px;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1">
                <div class="row">
                    <div class="col-12 d-flex justify-content-between">
                        <h1>Crear Nueva Feria</h1>
                        <div class="col-2">
                            <a href="{{route('fairs.index')}}" class="btn btn-primary">
                                <i class="fa fa-list"></i>
                                Lista
                            </a>
                        </div>
                    </div>
                </div>
                <form class="validate-form mb-5" method="POST" action="{{route('fairs.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <label for="">
                                @if(App::islocale('es'))
                                    <i class="fa fa-flag text-success"></i>
                                @endif
                                @if(App::islocale('en'))
                                    <i class="fa fa-flag text-info"></i>
                                @endif
                                Nombre
                            </label>
                            <input type="text" class="form-control mt-3 @error('name') is-invalid @enderror" placeholder="Nombre" required name="name" value="{{old('name')}}">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-12 col-md-6">
                            <label for="">URL</label>
                            <input type="text" class="form-control mt-3 @error('url') is-invalid @enderror" placeholder="Url" required name="url" value="{{old('url')}}">
                            @error('url')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <label for="">Fecha de Inicio</label>
                            <input type="date" class="form-control mt-3 @error('startDate') is-invalid @enderror"
                                   onkeydown="return false"
                                   placeholder="Fecha Inicio"
                                   required name="startDate" value="{{old('startDate')}}">
                            @error('startDate')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-12 col-md-6">
                            <label for="">Fecha de fin</label>
                            <input type="date" class="form-control mt-3 @error('endDate') is-invalid @enderror"
                                   onkeydown="return false"
                                   placeholder="Fecha Fin"
                                   required name="endDate" value="{{old('endDate')}}">
                            @error('endDate')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <label for="">Ramo</label>
                            <select name="sector" id=""
                                    class="form-control @error('sector') is-invalid @enderror">
                                @foreach($sectors as $sector)
                                    <option value="{{$sector->id}}">{{$sector->name}}</option>
                                @endforeach
                            </select>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-12 col-md-6">
                            <label for="">
                                @if(App::islocale('es'))
                                    <i class="fa fa-flag text-success"></i>
                                @endif
                                @if(App::islocale('en'))
                                    <i class="fa fa-flag text-info"></i>
                                @endif
                                Lugar
                            </label>
                            <input type="text" class="form-control mt-3 @error('place') is-invalid @enderror" placeholder="Lugar" name="place" value="{{old('place')}}">
                            @error('place')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label for="">
                                @if(App::islocale('es'))
                                    <i class="fa fa-flag text-success"></i>
                                @endif
                                @if(App::islocale('en'))
                                    <i class="fa fa-flag text-info"></i>
                                @endif
                                Descripción Breve
                            </label>
                            <textarea class="ckeditor mt-3 @error('short_description') is-invalid @enderror" placeholder="Descripción corta"
                                          name="short_description" value="{{old('short_description')}}">{{old('short_description')}}</textarea>
                            @error('short_description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <label for="">
                                @if(App::islocale('es'))
                                    <i class="fa fa-flag text-success"></i>
                                @endif
                                @if(App::islocale('en'))
                                    <i class="fa fa-flag text-info"></i>
                                @endif
                                Descripción
                            </label>
                                    <textarea class="ckeditor mt-3 @error('description') is-invalid @enderror bg-info" placeholder="Descripción"
                                          name="description" value="{{old('description')}}">{{old('description')}}</textarea>
                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <label for="">
                                @if(App::islocale('es'))
                                    <i class="fa fa-flag text-success"></i>
                                @endif
                                @if(App::islocale('en'))
                                    <i class="fa fa-flag text-info"></i>
                                @endif
                                Servicios
                            </label>
                                <textarea class="ckeditor mt-3 @error('services') is-invalid @enderror bg-info" placeholder="Servicios"
                                          name="services" value="{{old('services')}}"></textarea>
                            @error('services')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    <div class="row">
                        <div class="col-6">
                            <input type="text" class="form-control mt-3" placeholder="Video"  name="video">
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-4">
                            <label class="text-center">Poster</label>
                            <br>
                            <img src="{{asset('images/missing.png')}}" class="img-fluid" alt="Sin imagen" id="poster">
                        </div>
                        <div class="col-8">
                            <br><br>
                            <div class="alert alert-primary" role="alert">
                                <i class="fa fa-file-image"></i>
                                Poster (Formato png, jpg o jpeg 320 x 640 resolución 72 dpi max 900kb)<br>
                            </div>

                            <div class="upload-btn-wrapper" id="btnposter">
                                <button class="btn btn-primary">Selecciona un archivo</button>
                                <input type="file" name="poster" accept="image/*" onchange="loadFilePoster(event)">
                            </div>
                            <div id="deleteposter">
                                <a class="btn btn-danger" onclick="deletePoster()">Eliminar Imagen</a>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-4">
                            <label class="text-center">Logo</label>
                            <br>
                            <img src="{{asset('images/missing.png')}}" class="img-fluid" alt="Sin imagen" id="logo">
                        </div>
                        <div class="col-8">
                            <br><br>
                            <div class="alert alert-primary" role="alert">
                                <i class="fa fa-file-image"></i>
                                Logo (Formato png, jpg o jpeg 320 x 320 resolución 72 dpi max 900kb)<br>
                            </div>

                            <div class="upload-btn-wrapper" id="btnlogo">
                                <button class="btn btn-primary">Selecciona un archivo</button>
                                <input type="file" name="logo" accept="image/*" onchange="loadFileLogo(event)">
                            </div>
                            <div id="deletelogo">
                                <a class="btn btn-danger" onclick="deleteLogo()">Eliminar Imagen</a>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <div class="alert alert-info col-12" role="alert">
                                <i class="material-icons text-white">priority_high</i>
                                Fotos (formato png, jpg o jpeg 1024 x 768 resolución 72 dpi )
                            </div>
                            <dropzone-component :reference="''" type="fairs"></dropzone-component>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12 col-md-6">
                            <input type="text" placeholder="Latutid" class="form-control"
                                   name="latitude"
                                   id="latitude" value="">
                            @error('latitude')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-12 col-md-6">
                            <input type="text" placeholder="Longitud" class="form-control"
                                   name="longitude"
                                   id="longitude"
                                   value="" >
                            @error('longitude')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <input type="text" class="form-control mt-3"
                                   placeholder="Ubicación"
                                   id="location"
                                   required
                                   name="location">
                            @error('location')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div id="map" class="mt-3"></div>
                    <div class="row mt-5">
                        <div class="col-12">
                            <label for="leagues">Ligas</label>
                            <div class="border rounded p-3" id="leagues">
                                @foreach($leagues as $league)
                                    <div class="form-check">
                                        <input class="checkbox"
                                               value="{{$league->id}}"
                                               type="checkbox">
                                        <label class="form-check-label">
                                            {{ $league->name }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <input type="checkbox" class="checkbox" name="active" id="active" checked>
                            <label for="active">Activo</label>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-3 offset-9">
                            <button type="submit" class="btn btn-block btn-primary">
                                <i class="fa fa-save"></i>
                                Guardar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script type="text/javascript">
    document.getElementById('deleteposter').style.display = 'none';
    document.getElementById('deletelogo').style.display = 'none';
    function deletePoster(){
        document.getElementById('deleteposter').style.display = 'none';
        document.getElementById('btnposter').style.display = 'block';
        var image = document.getElementById('poster');
        image.src = '/images/missing.png';
        image.value = "";
    }
    function deleteLogo(){
        document.getElementById('deletelogo').style.display = 'none';
        document.getElementById('btnlogo').style.display = 'block';
        var image = document.getElementById('logo');
        image.src = '/images/missing.png';
        image.value = "";
    }
    var loadFilePoster = function(event) {
        var reader = new FileReader();
        reader.onload = function(){
            var image = document.getElementById('poster');
            image.src = reader.result;
        };
        reader.readAsDataURL(event.target.files[0]);
        document.getElementById('btnposter').style.display = 'none';
        document.getElementById('deleteposter').style.display = 'block';
    };
    var loadFileLogo = function(event) {
        var reader = new FileReader();
        reader.onload = function(){
            var image = document.getElementById('logo');
            image.src = reader.result;
        };
        reader.readAsDataURL(event.target.files[0]);
        document.getElementById('btnlogo').style.display = 'none';
        document.getElementById('deletelogo').style.display = 'block';
    };
    var map;
    var geocoder;
    // window.addEventListener("load", loadingPage);
    // function loadingPage(){
    //     var text_latitude = document.getElementById("latitude");
    //     text_latitude.value = latitude;
    //     var text_longitude = document.getElementById("longitude");
    //     text_longitude.value = longitude;
    // }

    function updateMarkerPosition(latLng) {
        var text_latitude = document.getElementById("latitude");
        var text_longitude = document.getElementById("longitude");
        text_latitude.value = latLng.lat();
        text_longitude.value = latLng.lng();
    }

    function updateMarkerAddress(str) {
        //document.getElementById('address').innerHTML = str;
        var text_address = document.getElementById('location');
        text_address.value = str;
    }

    function geocodePosition(pos) {
        geocoder.geocode({
            latLng: pos
        }, function(responses) {
            if (responses && responses.length > 0) {
                updateMarkerAddress(responses[0].formatted_address);
            } else {
                updateMarkerAddress('Cannot determine address at this location.');
            }
        });
    }

    function initMap() {
        var mapLayer = document.getElementById("map");
        var centerCoordinates = new google.maps.LatLng('19.432635251663157', '-99.13232105331417');
        var defaultOptions = { center: centerCoordinates, zoom: 8 }

        map = new google.maps.Map(mapLayer, defaultOptions);
        geocoder = new google.maps.Geocoder();

        geocoder.geocode( { 'address': 'Centro Histórico de la Cdad. de México' }, function(LocationResult, status) {
            // if (status == google.maps.GeocoderStatus.OK) {
            //     var latitude = LocationResult[0].geometry.location.lat();
            //     var longitude = LocationResult[0].geometry.location.lng();
            // }
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(19.432635251663157, -99.13232105331417),
                map: map,
                title: 'KomexTours',
                draggable: true,
                clickable: true,
                animation: google.maps.Animation.DROP
            });
            // Update current position info.
            updateMarkerPosition(marker.getPosition());
            geocodePosition(marker.getPosition());

            // Add dragging event listeners.
            google.maps.event.addListener(marker, 'dragstart', function() {
                updateMarkerAddress('Dragging...');
            });

            google.maps.event.addListener(marker, 'drag', function() {
                updateMarkerPosition(marker.getPosition());
            });

            google.maps.event.addListener(marker, 'dragend', function() {
                geocodePosition(marker.getPosition());
            });
        });
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8_8kSrBymZR_OLHLeCfWZFrX7l1HNUTE&callback=initMap"></script>
@endsection
