<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'KomexTours') }}</title>

    <!-- Scripts -->
    <script src="{{asset('js/jquery-3.5.1.min.js')}}"></script>
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

    <script src="{{ asset('js/app.js') }}" defer></script>

    <link rel="icon" href="{{ asset('favicon.ico') }}">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
{{--    <script type="text/javascript" src="http://download.skype.com/share/skypebuttons/js/skypeCheck.js">--}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    </script>
    @yield('head')
    @yield('codes')
    @yield('styles')
</head>
<body>
    @if(App::islocale('es'))
        <script async defer src="https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v3.2"></script>
    @endif
    @if(App::islocale('en'))
        <script async defer src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2"></script>
    @endif
    <div id="app">
        @include('public.layouts.header')
        <div class="container-fluid content-komex">
            <div class="row">
                <div class="col-12 col-md-3 -no-padding">
                    @include('public.layouts.siderbar')
                </div>
                <div class="col-12 col-md-9 mt-5">
                    @yield('content')
                </div>
            </div>

        </div>
        @include('public.layouts.footer')
    </div>
    @yield('javascript')
</body>
<script src="{{ asset('js/map.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(window).ready(function (){
        var images = $(".image");

        $(images).on("error", function(event) {
            $(event.target).css("display", "none");
        });
    });
</script>
</html>
