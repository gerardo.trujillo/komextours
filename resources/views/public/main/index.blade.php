@extends('public.layouts.index')

@section('content')
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            @foreach($promotions as $i => $prmotion)
                <li data-target="#carouselExampleIndicators" data-slide-to="{{$i}}" class="{{$i == 0 ? 'active':''}}"></li>
            @endforeach
        </ol>
        <div class="carousel-inner">
            @foreach($promotions as $key => $promotion)
                <div class="carousel-item {{$key == 0 ? 'active':''}}">
                    @if($promotion->url)
                        <a href="{{ $promotion->url }}" target="_blank">
                            <img src="{{asset($promotion->image)}}" class="d-block w-100" alt="{{$promotion->image}}">
                        </a>
                    @else
                        <a href="{{ route('promotion', ['promotion' => $promotion->id]) }}">
                            <img src="{{asset($promotion->image)}}" class="d-block w-100" alt="{{$promotion->image}}">
                        </a>
                    @endif
                </div>
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <div class="jumbotron jumbotron-fluid">
        <div class="container-fluid content-komex">
            <div class="row">
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="card-jumbotron p-3">
                        <img src="{{asset('images/ferias.jpeg')}}" class="card-img-top" alt="Ferias">
                        <div class="row px-3 py-2 card-index-title">
                            <h5 class="title-card -font-size-card">@lang('main.fairs.title')</h5>
                        </div>
                        <div class="row px-3">
                            <p class="card-text text-left card-index">
                                {!! Str::words(trans('main.fairs.content'), 11, '...') !!}
                            </p>
                        </div>
                        <div class="row px-3 py-2 d-flex justify-content-end">
                            <a href="{{ route('events') }}" class="btn btn-primary btn-sm align-right"> @lang('main.more')</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="card-jumbotron p-3">
                        <img src="{{asset('images/exten.jpeg')}}" class="card-img-top" alt="Ferias">
                        <div class="row px-3 py-2 card-index-title">
                            <h5 class="title-card -font-size-card">@lang('main.extensions.title')</h5>
                        </div>
                        <div class="row px-3">
                            <p class="card-text text-left card-index">
                                {!! Str::words(trans('main.extensions.content'), 11, '...') !!}
                            </p>
                        </div>
                        <div class="row px-3 py-2 d-flex justify-content-end">
                            <a href="{{ route('extensions') }}" class="btn btn-primary btn-sm align-right"> @lang('main.more')</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="card-jumbotron p-3">
                        <img src="{{asset('images/viajes.jpeg')}}" class="card-img-top" alt="Ferias">
                        <div class="row px-3 py-2 card-index-title">
                            <h5 class="title-card -font-size-card">@lang('main.tours.title')</h5>
                        </div>
                        <div class="row px-3">
                            <p class="card-text text-left card-index">
                                {!! Str::words(trans('main.tours.content'), 11, '...') !!}
                            </p>
                        </div>
                        <div class="row px-3 py-2 d-flex justify-content-end">
                            <a href="{{ route('services', ['item' => ' ']) }}" class="btn btn-primary btn-sm align-right"> @lang('main.more')</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="card-jumbotron p-3">
                        <img src="{{asset('images/conve.jpeg')}}" class="card-img-top" alt="Ferias">
                        <div class="row px-3 py-2 card-index-title">
                            <h5 class="title-card -font-size-card">@lang('main.conve.title')</h5>
                        </div>
                        <div class="row px-3">
                            <p class="card-text text-left card-index">
                                {!! Str::words(trans('main.conve.content'), 11, '...') !!}
                            </p>
                        </div>
                        <div class="row px-3 py-2 d-flex justify-content-end">
                            <a href="{{ route('services', ['item' => 'convenciones']) }}" class="btn btn-primary btn-sm align-right"> @lang('main.more')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($fairs)
        <div class="container-fluid content-komex">
            <h1 class="text-center text-bold text-green titles">@lang('main.fairsnext.title')</h1>
            <div class="row mt-3 pb-5">
                @foreach($fairs->sortBy('startDate') as $keyF => $fair)
                    <div class="col-12 col-md-6  mt-3 mt-mb-0 py-2 ">
                        <div class="row py-3 mx-md-1 bg-card border-card">
                            <div class="col-12 col-md-5">
                                @if($fair->poster)
                                    <img src="{{asset($fair->poster)}}" class="img-fluid -poster-index" alt="{{$fair->poster}}" id="poster">
                                @else
                                    <img src="{{asset('images/missing.png')}}" class="img-fluid -poster-index" alt="Sin imagen" id="poster">
                                @endif
                            </div>
                            <div class="col-12 col-md-7">
                                <div class="row">
                                    <div class="col-12">
                                        <h3 class="title-card titles">{{$fair->name}}</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <p class="h6 text-bold"><span class="title-card">@lang('main.fairsnext.star'):</span> {{ \Carbon\Carbon::parse($fair->startDate)->translatedFormat('d F Y') }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <p class="h6 text-bold"><span class="title-card">@lang('main.fairsnext.place'):</span> {{$fair->place}}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <p class="h6 text-bold">{!! Str::words($fair->short_description , 10, '...') !!}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <a href="{{ route('event', ['slug' => $fair->slug] ) }}" class="btn btn-primary btn-sm align-right mt-3">@lang('main.more')</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
    <div class="container-fluid content-komex">
        <div class="row">
            <div class="col-12 col-xxl-9">
                @include('public.main.components.testimonials')
            </div>

        </div>
    </div>

@endsection
{{--1*gL1x3s--}}
