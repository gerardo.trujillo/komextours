@extends('public.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-capitalize text-success font-weight-bold titles">@lang('services.title')</h1>
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link {{ $item == ' ' ? 'active':'' }}" id="ferias-tab" data-toggle="tab" href="#ferias" role="tab" aria-controls="ferias" aria-selected="true">@lang('services.fairs.title')</a> <!-- Ferias Internacionales – Viajes de Negocios a la Medida -->
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="extenciones-tab" data-toggle="tab" href="#extenciones" role="tab" aria-controls="extenciones" aria-selected="false">@lang('services.tours.title')</a> <!-- 2.-Extensiones & Servicios Turísticos en general -->
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="especiales-tab" data-toggle="tab" href="#especiales" role="tab" aria-controls="especiales" aria-selected="false">@lang('services.specialTrips.title')</a> <!-- 3.-Viajes especiales para Grupos particulares -->
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="mexico-tab" data-toggle="tab" href="#mexico" role="tab" aria-controls="mexico" aria-selected="false">@lang('services.passion.title')</a> <!-- 4.-Passion for Mexico – Turismo Receptivo de alta calidad -->
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link {{ $item == 'convenciones' ? 'active':'' }}" id="incentivos-tab" data-toggle="tab" href="#incentivos" role="tab" aria-controls="incentivos" aria-selected="false">@lang('services.conventions.title')</a> <!-- 5.-Convenciones e Incentivos -->
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="management-tab" data-toggle="tab" href="#management" role="tab" aria-controls="management" aria-selected="false">@lang('services.destination.title')</a> <!-- 6.-Destination Management -->
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="concierge-tab" data-toggle="tab" href="#concierge" role="tab" aria-controls="concierge" aria-selected="false">@lang('services.concierge.title')</a> <!-- 7.-Concierge -->
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="protocolos-tab" data-toggle="tab" href="#protocolos" role="tab" aria-controls="protocolos" aria-selected="false">@lang('services.protocols.title')</a> <!-- 8.-Protocolos globales y Sello para la Nueva Normalidad -->
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="beneficios-tab" data-toggle="tab" href="#beneficios" role="tab" aria-controls="beneficios" aria-selected="false">@lang('services.benefits.title')</a> <!-- Sus Beneficios – Nuestra Garantía de Calidad -->
                    </li>
                </ul>

                <div class="tab-content pt-5">
                    <div class="tab-pane {{ $item == ' ' ? 'active':'' }}" id="ferias" role="tabpanel" aria-labelledby="ferias-tab">
                        <p class="text-left">
                            @lang('services.fairs.paragraph')
                            <br><br>
                            @lang('services.fairs.paragraph2')
                            <br><br>
                            @lang('services.fairs.paragraph3')
                        </p>
                        <div class="row d-flex justify-content-center">
                            <div class="col-12 col-md-6 col-lg-2">
                                <img src="{{ asset('images/internacional.jpg') }}" class="img-fluid" alt="international">
                            </div>
                            <div class="col-12 col-md-6 col-lg-2">
                                <img src="{{ asset('images/messe.png') }}" class="img-fluid" alt="messe">
                            </div>
                            <div class="col-12 col-md-6 col-lg-2">
                                <img src="{{ asset('images/hannover.jpg') }}" class="img-fluid" alt="hannover">
                            </div>
                            <div class="col-12 col-md-6 col-lg-2">
                                <img src="{{ asset('images/koln.jpg') }}" class="img-fluid" alt="koln">
                            </div>
                            <div class="col-12 col-md-6 col-lg-2">
                                <img src="{{ asset('images/reed-exhibitions.jpg') }}" class="img-fluid" alt="Reed Exhibitions">
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="extenciones" role="tabpanel" aria-labelledby="extenciones-tab">
                        <p class="text-left">
                            @lang('services.tours.paragraph')
                            <br><br>
                            @lang('services.tours.paragraph2')
                        </p>
                        <div class="row justify-content-center">
                            <div class="col-6 col-lg-4 col-xl-3 ">
                                <img src="{{ asset('images/calidad-turistica.jpg') }}" class="img-fluid" alt="calidad turistica">
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="especiales" role="tabpanel" aria-labelledby="especiales-tab">
                        <p class="text-left">
                            @lang('services.specialTrips.paragraph')
                        </p>
                        <div class="row justify-content-center">
                            <div class="col-6 col-lg-4 col-xl-3 ">
                                <img src="{{ asset('images/asist-card.jpg') }}" class="img-fluid" alt="assist-card">
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="mexico" role="tabpanel" aria-labelledby="mexico-tab">
                        <p class="text-left">
                            @lang('services.passion.paragraph')
                            <br><br>
                            @lang('services.passion.paragraph2')
                        </p>
                        
                    </div>
                    <div class="tab-pane {{ $item == 'convenciones' ? 'active':'' }}" id="incentivos" role="tabpanel" aria-labelledby="incentivos-tab">
                        <p class="text-left">
                            @lang('services.conventions.paragraph')
                        </p>
                        <div class="row justify-content-center">
                            <div class="col-6 col-lg-4 col-xl-3 ">
                                <img src="{{ asset('images/marca/Compra-con-tu-Agencia-de-Viajes.jpeg') }}" class="img-fluid" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="management" role="tabpanel" aria-labelledby="management-tab">
                        <p class="text-left">
                            @lang('services.destination.paragraph')
                        </p>
                        <div class="row justify-content-center">
                            <div class="col-6 col-lg-4 col-xl-3 ">
                                <img src="{{ asset('images/royaldmc.png') }}" class="img-fluid" alt="">
                            </div>
                            <div class="col-6 col-lg-4 col-xl-3 ">
                                <img src="{{ asset('images/CiudadMéxico.png') }}" class="img-fluid" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="concierge" role="tabpanel" aria-labelledby="concierge-tab">
                        <p class=text-text>
                            @lang('services.concierge.paragraph')
                            <br><br>
                            @lang('services.concierge.paragraph2')
                            <br><br>
                            @lang('services.concierge.paragraph3')
                            <br><br>
                            @lang('services.concierge.paragraph4')
                            <br><br>
                            @lang('services.concierge.paragraph5')
                            <br><br>
                            <i class="fa fa-check"></i>
                            @lang('services.concierge.paragraph6')
                            <br><br>
                            <i class="fa fa-check"></i>
                            @lang('services.concierge.paragraph7')
                            <br><br>
                            <i class="fa fa-check"></i>
                            @lang('services.concierge.paragraph8')
                            <br><br>
                            <i class="fa fa-check"></i>
                            @lang('services.concierge.paragraph9')
                            <br><br>
                            <i class="fa fa-check"></i>
                            @lang('services.concierge.paragraph10')
                            <br><br>
                            <i class="fa fa-check"></i>
                            @lang('services.concierge.paragraph11')
                            <br><br>
                            <i class="fa fa-check"></i>
                            @lang('services.concierge.paragraph12')
                            <br><br>
                            <i class="fa fa-check"></i>
                            @lang('services.concierge.paragraph13')
                            <br><br>
                            @lang('services.concierge.paragraph14')
                            <br><br>
                            @lang('services.concierge.paragraph15')
                            <br><br>
                            @lang('services.concierge.paragraph16')
                        </p>
                    </div>
                    <div class="tab-pane" id="protocolos" role="tabpanel" aria-labelledby="protocolos-tab">
                        <div class="noticia">
                            <img class="izquierda" src="{{ asset('images/safeTravels.png') }}" class="img-fluid" alt="Safe Travels">
                            <h3 class="title-card titles mb-5">@lang('services.protocols.subtitle')</h3>
                            <p class=text-text>@lang('services.protocols.paragraph')</p>
                            <div class="reset"></div>
                        </div>
                        <p class="text-text">
                            @lang('services.protocols.paragraph2')
                            <br><br>
                            @lang('services.protocols.paragraph3')
                            <br><br>
                            @lang('services.protocols.paragraph4')
                            <br><br>
                            
                        </p>
                    </div>
                    <div class="tab-pane" id="beneficios" role="tabpanel" aria-labelledby="beneficios-tab">
                        <p class="text-left">
                            @lang('services.benefits.paragraph')
                            <br><br>
                            @lang('services.benefits.paragraph2')
                            <br><br>
                            @lang('services.benefits.paragraph3')
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
