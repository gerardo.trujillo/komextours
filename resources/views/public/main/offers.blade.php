@extends('public.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        <form action="" class="group-form py-4">
                            <input type="text" class="form-control mb-3" placeholder="{{ trans('extensions.search') }}" id="search" onkeyup="change(this.value)">
                            <select name="sector" id="sector" class="form-control" onchange="change(this.value)">
                                <option value="">@lang('extensions.select')</option>
                                @foreach($regions as $region)
                                    <option value="{{$region->id}}">{{$region->region}}</option>
                                @endforeach
                            </select>
                        </form>
                    </div>
                </div>
                <h1 class="text-capitalize text-success font-weight-bold titles">@lang('offers.title')</h1>
                <hr>
                <div class="row" id="result">
                    <div class="col-12">
                        {{ $extensions->links() }}

                        @foreach($extensions as $extension)
                            <div class="row border-bottom border-success">
                                <div class="col-12 col-lg-3 py-3">
                                    @php($image = DB::table('images')->where('type', 'offers')->where('reference', $extension->id)->first())
                                    @if($image)
                                        <img src="{{asset($image->name)}}" class="img-fluid w-100 image" alt="{{$image->name}}" id="logo">
                                    @else
                                        <img src="{{asset('images/missing.png')}}" class="img-fluid w-100 image" alt="Sin imagen" id="logo">
                                    @endif

                                </div>
                                <div class="col-12 col-lg-9 py-3">
                                    <h4 class="text-info">{{ $extension->name }}</h4>
                                    <p class="text-left h6">
                                        {!! Str::words($extension->description , 40, ' ...') !!}
                                    </p>
                                    <h4>
                                        <span class="text-info">@lang('extensions.region'):</span>
                                        <span>{{$extension->region->region}}</span>
                                    </h4>
                                    <h4>
                                        <span class="text-info">@lang('extensions.destination'):</span>
                                        <span>{{$extension->destino->destination}}</span>
                                    </h4>
                                    <h4>
                                        <span class="text-info">@lang('extensions.category'):</span>
                                        <span>{{$extension->category->category}}</span>
                                    </h4>
                                    <a href="{{ route('offer', ['slug' => $extension->slug]) }}" class="btn btn-info align-right">@lang('fairs.more')</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script>
        function change(param){
            var result = document.getElementById('result');
            axios.post('/offers/component', {
                param: param,
            }).then(function (response) {
                // console.log(response);
                result.innerHTML = response.data;
            }).catch(function (error){
                console.log(error);
            });
        }
    </script>
@endsection
