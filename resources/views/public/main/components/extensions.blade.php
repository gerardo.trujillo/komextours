<div class="col-12">
    {{ $extensions->links() }}
    @forelse($extensions as $extension)
        <div class="row border-bottom border-success">
            <div class="col-12 col-lg-3 py-3">
                @if($extension->logo)
                    <img src="{{asset($extension->logo)}}" class="img-fluid w-100" alt="{{$extension->logo}}" id="logo">
                @else
                    <img src="{{asset('images/missing.png')}}" class="img-fluid w-100" alt="Sin imagen" id="logo">
                @endif
            </div>
            <div class="col-12 col-lg-9 py-3">
                <h4 class="text-info">{{ $extension->name }}</h4>
                <p class="text-left h6">
                    {!! Str::words($extension->description , 40, ' ...') !!}
                </p>
                <h4>
                    <span class="text-info">@lang('extensions.region'):</span>
                    <span>{{$extension->region->region}}</span>
                </h4>
                <h4>
                    <span class="text-info">@lang('extensions.destination'):</span>
                    <span>{{$extension->destino->destination}}</span>
                </h4>
                <h4>
                    <span class="text-info">@lang('extensions.category'):</span>
                    <span>{{$extension->category->category}}</span>
                </h4>
                <a href="#" class="btn btn-info align-right">@lang('fairs.more')</a>
            </div>
        </div>
    @empty
        <h1 class="text-center">@lang('extensions.empty')</h1>
    @endforelse
</div>
