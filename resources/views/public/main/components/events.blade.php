<div class="col-12">
    {{ $fairs->links() }}
    @forelse($fairs as $fair)
        <div class="row border-bottom border-success">
            <div class="col-12 col-lg-3 py-3">
                @if($fair->logo)
                    <img src="{{asset($fair->logo)}}" class="img-fluid w-100" alt="{{$fair->logo}}" id="logo">
                @else
                    <img src="{{asset('images/missing.png')}}" class="img-fluid w-100" alt="Sin imagen" id="logo">
                @endif
            </div>
            <div class="col-12 col-lg-9 py-3">
                <h4 class="text-info">{{ $fair->name }}</h4>
                <p class="text-left img-fluid w-100">{!! $fair->short_description !!}</p>
                <h6>
                    <span class="text-info">@lang('fairs.date')</span>
                    {{ \Carbon\Carbon::parse($fair->startDate)->translatedFormat('d F Y') }} a
                    {{ \Carbon\Carbon::parse($fair->endDate)->translatedFormat('d F Y') }}
                </h6>
                <h6>
                    <span class="text-info">@lang('fairs.place')</span>
                    {{$fair->place}}
                </h6>
                <h6>
                    <span class="text-info">@lang('fairs.sector')</span>
                    {{$fair->sector->name}}
                </h6>
                <a href="{{ route('event', ['slug' => $fair->slug] ) }}" class="btn btn-info align-right">@lang('fairs.more')</a>
            </div>
        </div>
    @empty
        <h1 class="text-center">@lang('fairs.empty')</h1>
    @endforelse
</div>
