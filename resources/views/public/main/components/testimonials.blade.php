<div class="jumbotron jumbotron-fluid">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">


            <div class="carousel-item active">
                <div class="row d-flex justify-content-center">
                    <div class="col-11">
                        <p class="titles strongText text-center">@lang('testimonials.component.title12')</p>
                        <p class="">@lang('testimonials.component.content12')</p>
                    </div>
                </div>
                <div class="row d-flex justify-content-center">
                    <div class="col-12 col-sm-8 col-md-6">
                        <div class="row d-flex justify-content-center">
                            <div class="col-12 col-md-6">
                                <a href="{{ route('testimonials') }}" class="btn btn-primary btn-block">@lang('testimonials.component.all')</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="carousel-item ">
                <div class="row d-flex justify-content-center">
                    <div class="col-11">
                        <p class="titles strongText text-center">@lang('testimonials.component.title1')</p>
                        <p class="">@lang('testimonials.component.content1')</p>
                    </div>
                </div>
                <div class="row d-flex justify-content-center">
                    <div class="col-12 col-sm-8 col-md-6">
                        <div class="row d-flex justify-content-center">
                            <div class="col-12 col-md-6">
                                <a href="{{ route('testimonials') }}" class="btn btn-primary btn-block">@lang('testimonials.component.all')</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="carousel-item">
                <div class="row d-flex justify-content-center">
                    <div class="col-11">
                        <p class="titles strongText text-center">@lang('testimonials.component.title2')</p>
                        <p class="">@lang('testimonials.component.content2')</p>
                    </div>
                </div>
                <div class="row d-flex justify-content-center">
                    <div class="col-12 col-sm-8 col-md-6">
                        <div class="row d-flex justify-content-center">
                            <div class="col-12 col-md-6">
                                <a href="{{ route('testimonials') }}" class="btn btn-primary btn-block">@lang('testimonials.component.all')</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
