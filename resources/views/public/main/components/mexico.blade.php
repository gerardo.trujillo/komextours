<div class="col-12">
    {{ $extensions->links() }}
    @forelse($extensions as $extension)
        <div class="row border-bottom border-success">
            <div class="col-12 col-lg-3 py-3">
                @php($image = DB::table('images')->where('type', 'mexico')->where('reference', $extension->id)->first())
                <a href="{{ route('mex', ['slug' => $extension->slug]) }}">
                    @if($image)
                        <img src="{{asset($image->name)}}" class="img-fluid w-100 image" alt="{{$image->name}}" id="logo">
                    @else
                        <img src="{{asset('images/missing.png')}}" class="img-fluid w-100 image" alt="Sin imagen" id="logo">
                    @endif
                </a>
            </div>
            <div class="col-12 col-lg-9 py-3">
                <h4 class="title-card titles">{{ $extension->name }}</h4>
                <p>
                    <span class="title-card">@lang('extensions.region'):</span>
                    @if($extension->region)<span>{{$extension->region->region}}</span> @endif <br>
                    <span class="title-card">@lang('extensions.destination'):</span>
                    @if($extension->destinoMexico)<span>{{$extension->destinoMexico->region}}</span>@endif <br>
                </p>
                <p class="text-left">
                    {!! Str::words($extension->description , 40, ' ...') !!}
                </u>
                </p>
                <google-map-component :longitude="{{$extension->longitude}}" :latitude="{{$extension->latitude}}"></google-map-component>
                <a href="{{ route('mex', ['slug' => $extension->slug]) }}" class="btn btn-primary align-right mt-3">@lang('fairs.more')</a>
            </div>
        </div>
    @empty
        <h1 class="text-center">@lang('extensions.mexico.empty')</h1>
    @endforelse
</div>
<script>
    $(document).ready(function(){
        alert('ima');
        $('img').each(function(){
            if($(this)[0].naturalHeight == 0){
                $(this).attr('src','images/missing.png');
            }
        })
    });
</script>
