@extends('public.layouts.app')


@section('head')
    <meta property="og:url"           content="{{ config('app.url') }}/{{$url}}/{{$extension->slug}}" />
    <meta property="og:type"          content="{{ $url }}" />
    <meta property="og:title"         content="{{ $extension->name }}" />
    <meta property="og:description"   content="{{ $description }}" />
    @if($image)
        <meta property="og:image"         content="{{ config('app.url') }}/{{$image->name}}" />
        @else
        <meta property="og:image"         content="{{ config('app.url') }}/images/missing.png" />
    @endif
@endsection

{{--@section('styles')--}}
{{--    <style type="text/css">--}}
{{--        #map {--}}
{{--            width:100%;--}}
{{--            height: 500px;--}}
{{--        }--}}
{{--    </style>--}}
{{--@endsection--}}

@section('content')
    @if(App::islocale('es'))
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v9.0&appId=463005901528543&autoLogAppEvents=1" nonce="ykb4vpSp"></script>
    @endif
    @if(App::islocale('en'))
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v9.0&appId=463005901528543&autoLogAppEvents=1" nonce="ykb4vpSp"></script>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-green titles">{{$extension->name}}</h1>
                @if($image)
                    <a data-fancybox="gallery" href="{{ $image->name }}">
                        <img src="{{asset($image->name)}}" class="img-fluid w-25 image" alt="{{$image->name}}" id="logo">
                    </a>
                @else
                    <img src="{{asset('images/missing.png')}}" class="img-fluid w-25 image" alt="Sin imagen" id="poster">
                @endif
                <p class="mt-3">
                    <span class="title-card font-bold">@lang('extensions.show.region')</span>
                    {{ $extension->region->region }} <br>
{{--                    <span class="title-card font-bold">@lang('extensions.show.date')</span>--}}
{{--                    <span class="tex-capitalize">--}}
{{--                        {{ \Carbon\Carbon::parse($extension->startDate)->translatedFormat('d F Y') }} a--}}
{{--                        {{ \Carbon\Carbon::parse($extension->endDate)->translatedFormat('d F Y') }}--}}
{{--                    </span> <br>--}}
                    @if($extension->category->category != '')
                        <span class="title-card" >@lang('extensions.show.category')</span>
                        <span>{{ $extension->category->category }}</span>
                    @endif
                </p>
                <div class="container mt-3">
                    <div class="row">
                        @foreach($images as $keyg => $gallery)
                            @if($gallery->id != $image->id)
                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 mt-4">
                                    <a data-fancybox="gallery" href="{{ $gallery->name }}">
                                        <img src="{{asset($gallery->name)}}" class="img-fluid image" alt="{{$gallery->name}}" id="Imagen de galeria {{ $keyg + 1 }}">
                                    </a>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="maxWidth">
                    @if(!empty($extension->description))
                        <p class="title-card font-bold">@lang('extensions.show.description')</p>
                        <p>{!! $extension->description !!}</p>
                    @endif
                    @if(!empty($extension->services))
                        <p class="title-card font-bold">@lang('extensions.show.services')</p>
                        <p>{!! $extension->services !!}</p>
                    @endif
                    @if(!empty($extension->facilities))
                        <p class="title-card font-bold">@lang('extensions.show.facilities')</p>
                        <p>{!! $extension->facilities !!}</p>
                    @endif
                    @if($extension->itinerary_file)
                        <a href="{{ $extension->itinerary_file }}" class="btn btn-primary" target="_blank">@lang('extensions.show.itinerary_file')</a>
                    @endif
                </div>
{{--                @if(!empty($extension->rates))--}}
{{--                    <p class="title-card font-bold">@lang('extensions.show.rates')</p>--}}
{{--                    <p>{!! $extension->rates !!}</p>--}}
{{--                @endif--}}
{{--                <p>--}}
{{--                    <span class="title-card font-bold">@lang('extensions.show.place')</span>--}}
{{--                    <span class="font-bold">{{$extension->location}}</span>--}}
{{--                </p>--}}
{{--                <div class="row">--}}
{{--                    <div class="col-12 mt-5" id="map"></div>--}}
{{--                </div>--}}
                @if($extension->simple_cash || $extension->doble_cash || !empty($extension->triple_cash))
                    @if(!empty($extension->simple_cash))
                        <p class="title-card font-bold">@lang('fairs.show.cash1')</p>
                        <p>{!! $extension->simple_cash !!}</p>
                    @endif
                        @if(!empty($extension->doble_cash))
                            <p class="title-card font-bold">@lang('fairs.show.cash2')</p>
                            <p>{!! $extension->doble_cash !!}</p>
                        @endif
                        @if(!empty($extension->triple_cash))
                            <p class="title-card font-bold">@lang('fairs.show.cash3')</p>
                            <p>{!! $extension->triple_cash !!}</p>
                        @endif
                @endif
                @if($extension->simple_card || $extension->doble_card || !empty($extension->triple_card))
                    @if(!empty($extension->simple_card))
                        <p class="title-card font-bold">@lang('fairs.show.card1')</p>
                        <p>{!! $extension->simple_card !!}</p>
                    @endif
                    @if(!empty($extension->doble_card))
                        <p class="title-card font-bold">@lang('fairs.show.card2')</p>
                        <p>{!! $extension->doble_card !!}</p>
                    @endif
                    @if(!empty($extension->triple_card))
                        <p class="title-card font-bold">@lang('fairs.show.card3')</p>
                        <p>{!! $extension->triple_card !!}</p>
                    @endif
                @endif
    @if($extension->pack_city || $extension->museum_package || !empty($extension->a_circuit) || !empty($extension->circuit_days) || !empty($extension->food_plan)
|| !empty($extension->lenguage_plan) || !empty($extension->cruise_from_to) || !empty($extension->go_suitable_trnasfer) || !empty($extension->cruise_days)
|| !empty($extension->go_transfer_from_to) || !empty($extension->train_tickets) || !empty($extension->train_tickets_from_to) || !empty($extension->ferry)
|| !empty($extension->car_rental) || !empty($extension->rent_car_days) || !empty($extension->walk) || !empty($extension->foods) || !empty($extension->dinners)
|| !empty($extension->excursion_1) || !empty($extension->excursion_2) || !empty($extension->excursion_3) || !empty($extension->entry) || !empty($extension->passes)
|| !empty($extension->see_another_1) || !empty($extension->see_another_2) || !empty($extension->see_another_3))
        <div class="row mt-5">
            <div class="col-12">
                <p class="title-card">@lang('extensions.show.inclusions.title')</p>
                <div class="border rounded p-3" id="special_travels">
                    @if($extension->pack_city || $extension->museum_package)
                    <div class="row my-3">
                        <div class="col-6">
                            @if($extension->pack_city)
                                <p><i class="fas fa-check text-green"></i> @lang('extensions.show.inclusions.city')</p>
                            @endif
                        </div>
                        <div class="col-6">
                            @if($extension->museum_package)
                                <p><i class="fas fa-check text-green"></i> @lang('extensions.show.inclusions.museum')</p>
                            @endif
                        </div>
                    </div>
                    @endif
                    @if($extension->a_circuit || $extension->circuit_days || $extension->food_plan || $extension->lenguage_plan)
                    <div class="row my-3">
                        @if(!empty($extension->a_circuit))
                            <div class="col-12 col-md-3">
                                <span class="title-card">
                                    @lang('extensions.show.inclusions.circuitFrom')
                                </span><br>
                                {{ $extension->a_circuit }}
                            </div>
                        @endif

                            @if(!empty($extension->circuit_days))
                                <div class="col-12 col-md-3">
                                    <p> <span class="title-card">@lang('extensions.show.inclusions.circuitDays')</span><br>
                                    {{ $extension->circuit_days }}
                                    </p>
                                </div>
                            @endif

                            @if(!empty($extension->food_plan))
                                <div class="col-12 col-md-3">
                                    <p><span class="title-card">@lang('extensions.show.inclusions.meal')</span> <br>
                                        {{ $extension->food_plan }}
                                    </p>
                                </div>
                            @endif
                            @if(!empty($extension->lenguage_plan))
                                <div class="col-12 col-md-3">
                                    <p><span class="title-card">@lang('extensions.show.inclusions.language')</span> <br>
                                        {{ $extension->lenguage_plan }}
                                    </p>
                                </div>
                            @endif
                        </div>
                    @endif
                    @if($extension->cruise_from_to || $extension->go_suitable_trnasfer || $extension->cruise_days || $extension->go_transfer_from_to)
                    <div class="row my-3">
                        @if(!empty($extension->cruise_from_to))
                            <div class="col-12 col-md-3">
                                <p><span class="title-card">@lang('extensions.show.inclusions.cruiseFrom')</span> <br>
                                    {{ $extension->cruise_from_to }}
                                </p>
                            </div>
                        @endif
                            @if(!empty($extension->go_suitable_trnasfer))
                                <div class="col-12 col-md-3">
                                    <p><span class="title-card">@lang('extensions.show.inclusions.CruiseDays')</span> <br>
                                        {{ $extension->go_suitable_trnasfer }}
                                    </p>
                                </div>
                            @endif
                            @if(!empty($extension->cruise_days))
                                <div class="col-12 col-md-3">
                                    <p><span class="title-card">@lang('extensions.show.inclusions.AirTransfer')</span> <br>
                                        {{ $extension->cruise_days }}
                                    </p>
                                </div>
                            @endif
                            @if(!empty($extension->go_transfer_from_to))
                                <div class="col-12 col-md-3">
                                    <p><span class="title-card">@lang('extensions.show.inclusions.transferFrom')</span> <br>
                                        {{ $extension->go_transfer_from_to }}
                                    </p>
                                </div>
                            @endif
                    </div>
                    @endif
                    @if($extension->train_tickets || $extension->train_tickets_from_to || $extension->ferry)
                    <div class="row my-3">
                        @if(!empty($extension->train_tickets))
                            <div class="col-12 col-md-3">
                                <p><span class="title-card">@lang('extensions.show.inclusions.class')</span> <br>
                                    {{ $extension->train_tickets }}
                                </p>
                            </div>
                        @endif
                            @if(!empty($extension->train_tickets_from_to))
                                <div class="col-12 col-md-3">
                                    <p><span class="title-card">@lang('extensions.show.inclusions.train')</span> <br>
                                        {{ $extension->train_tickets_from_to }}
                                    </p>
                                </div>
                            @endif
                            @if(!empty($extension->ferry))
                                <div class="col-12 col-md-3">
                                    <p><span class="title-card">@lang('extensions.show.inclusions.ferry')</span> <br>
                                        {{ $extension->ferry }}
                                    </p>
                                </div>
                            @endif
                    </div>
                        @endif
                        @if($extension->car_rental || $extension->rent_car_days)
                    <div class="row my-3">
                        @if(!empty($extension->car_rental))
                            <div class="col-12 col-md-3">
                                <p><span class="title-card">@lang('extensions.show.inclusions.carFree')</span> <br>
                                    {{ $extension->car_rental }}
                                </p>
                            </div>
                        @endif
                            @if(!empty($extension->rent_car_days))
                                <div class="col-12 col-md-3">
                                    <p><span class="title-card">@lang('extensions.show.inclusions.car')</span> <br>
                                        {{ $extension->rent_car_days }}
                                    </p>
                                </div>
                            @endif
                    </div>
                        @endif
                        @if($extension->walk || $extension->foods || $extension->dinners)
                    <div class="row my-3">
                        @if(!empty($extension->walk))
                            <div class="col-12 col-md-3">
                                <p><span class="title-card">@lang('extensions.show.inclusions.walk')</span> <br>
                                    {{ $extension->walk }}
                                </p>
                            </div>
                        @endif
                            @if(!empty($extension->foods))
                                <div class="col-12 col-md-3">
                                    <p><span class="title-card">@lang('extensions.show.inclusions.food')</span> <br>
                                        {{ $extension->foods }}
                                    </p>
                                </div>
                            @endif
                            @if(!empty($extension->dinners))
                                <div class="col-12 col-md-3">
                                    <p><span class="title-card">@lang('extensions.show.inclusions.dinners')</span> <br>
                                        {{ $extension->dinners }}
                                    </p>
                                </div>
                            @endif
                    </div>
                        @endif
                        @if($extension->excursion_1 || $extension->excursion_2 || $extension->excursion_3)
                    <div class="row my-3">
                        @if(!empty($extension->excursion_1))
                            <div class="col-12 col-md-3">
                                <p><span class="title-card">@lang('extensions.show.inclusions.excursion1'):</span> <br>
                                    {{ $extension->excursion_1 }}
                                </p>
                            </div>
                        @endif
                            @if(!empty($extension->excursion_2))
                                <div class="col-12 col-md-3">
                                    <p><span class="title-card">@lang('extensions.show.inclusions.excursion2')</span> <br>
                                        {{ $extension->excursion_2 }}
                                    </p>
                                </div>
                            @endif
                            @if(!empty($extension->excursion_3))
                                <div class="col-12 col-md-3">
                                    <p><span class="title-card">@lang('extensions.show.inclusions.excursion3')</span> <br>
                                        {{ $extension->excursion_3 }}
                                    </p>
                                </div>
                            @endif
                    </div>
                        @endif
                        @if($extension->entry || $extension->entry)
                    <div class="row my-3">
                        @if(!empty($extension->entry))
                            <div class="col-12 col-md-3">
                                <p><span class="title-card">@lang('extensions.show.inclusions.entry')</span> <br>
                                    {{ $extension->entry }}
                                </p>
                            </div>
                        @endif
                            @if(!empty($extension->entry))
                                <div class="col-12 col-md-3">
                                    <p><span class="title-card">@lang('extensions.show.inclusions.passes'):</span> <br>
                                        {{ $extension->passes }}
                                    </p>
                                </div>
                            @endif
                    </div>
                        @endif
                        @if($extension->see_another_1 || $extension->see_another_2 || $extension->see_another_3)
                    <div class="row my-3">
                        @if(!empty($extension->see_another_1))
                            <div class="col-12 col-md-3">
                                <p><span class="title-card">@lang('extensions.show.inclusions.other1')</span> <br>
                                    {{ $extension->see_another_1 }}
                                </p>
                            </div>
                        @endif
                            @if(!empty($extension->see_another_2))
                                <div class="col-12 col-md-3">
                                    <p><span class="title-card">@lang('extensions.show.inclusions.other2')</span> <br>
                                        {{ $extension->see_another_2 }}
                                    </p>
                                </div>
                            @endif
                            @if(!empty($extension->see_another_3))
                                <div class="col-12 col-md-3">
                                    <p><span class="title-card">@lang('extensions.show.inclusions.other2')</span> <br>
                                        {{ $extension->see_another_3 }}
                                    </p>
                                </div>
                            @endif
                    </div>
                        @endif
                </div>
            </div>
        </div>
    @endif
{{--    @if(!empty($extension->guide) || !empty($extension->local_guide) || $extension->trips || !empty($extension->sure) || !empty($extension->health_insurance)--}}
{{--|| !empty($extension->visa_for) || $extension->assistance || $extension->trunks || $extension->charges || $extension->taxes || $extension->emergency || $extension->local_contact--}}
{{--|| $extension->assistenace || $extension->folders || $extension->destination || $extension->itinerary || $extension->exhibitor_tips)--}}
{{--        <div class="row mt-5">--}}
{{--            <div class="col-12">--}}
{{--                <p class="title-card">General</p>--}}
{{--                <div class="border rounded p-3" id="general">--}}
{{--                    @if($extension->guide || $extension->local_guid || $extension->sure || $extension->health_insurance || $extension->visa_for)--}}
{{--                    <div class="row my-3">--}}
{{--                        @if(!empty($extension->guide))--}}
{{--                            <div class="col-12 col-md-3">--}}
{{--                                <p><span class="title-card">Guía acompañante / de - a:</span> <br>--}}
{{--                                    {{ $extension->guide }}--}}
{{--                                </p>--}}
{{--                            </div>--}}
{{--                        @endif--}}
{{--                            @if(!empty($extension->local_guid))--}}
{{--                                <div class="col-12 col-md-3">--}}
{{--                                    <p><span class="title-card">Guía local de idioma:</span> <br>--}}
{{--                                        {{ $extension->local_guide }}--}}
{{--                                    </p>--}}
{{--                                </div>--}}
{{--                            @endif--}}
{{--                            @if(!empty($extension->sure))--}}
{{--                                <div class="col-12 col-md-3">--}}
{{--                                    <p><span class="title-card">Seguro de Cancelación:</span> <br>--}}
{{--                                        {{ $extension->sure }}--}}
{{--                                    </p>--}}
{{--                                </div>--}}
{{--                            @endif--}}
{{--                            @if(!empty($extension->health_insurance))--}}
{{--                                <div class="col-12 col-md-3">--}}
{{--                                    <p><span class="title-card">Seguro de enfermedad y accidentes:</span> <br>--}}
{{--                                        {{ $extension->health_insurance }}--}}
{{--                                    </p>--}}
{{--                                </div>--}}
{{--                            @endif--}}
{{--                            @if(!empty($extension->visa_for))--}}
{{--                                <div class="col-12 col-md-3">--}}
{{--                                    <p><span class="title-card">Visado para:</span> <br>--}}
{{--                                        {{ $extension->visa_for }}--}}
{{--                                    </p>--}}
{{--                                </div>--}}
{{--                            @endif--}}
{{--                    </div>--}}
{{--                    @endif--}}
{{--                    @if($extension->assistance || $extension->trunks || $extension->charges || $extension->taxes || $extension->emergency--}}
{{-- || $extension->local_contact || $extension->assistenace || $extension->folders || $extension->destination || $extension->itinerary--}}
{{--  || $extension->exhibitor_tips || $extension->tips)--}}
{{--                    <div class="row mt-3">--}}
{{--                        @if($extension->assistance)--}}
{{--                            <div class="col-12 col-md-3">--}}
{{--                                <p><i class="fas fa-check text-green"></i>  Asistencia en el destino--}}
{{--                                </p>--}}
{{--                            </div>--}}
{{--                        @endif--}}
{{--                            @if($extension->trunks)--}}
{{--                                <div class="col-12 col-md-3">--}}
{{--                                    <p><i class="fas fa-check text-green"></i>  Servicio de maleteros en aeropuertos--}}
{{--                                    </p>--}}
{{--                                </div>--}}
{{--                            @endif--}}
{{--                        @if($extension->charges)--}}
{{--                            <div class="col-12 col-md-3">--}}
{{--                                <p><i class="fas fa-check text-green"></i>  Cargos por Servicios--}}
{{--                                </p>--}}
{{--                            </div>--}}
{{--                        @endif--}}
{{--                            @if($extension->taxes)--}}
{{--                                <div class="col-12 col-md-3">--}}
{{--                                    <p><i class="fas fa-check text-green"></i>  Todos los impuestos--}}
{{--                                    </p>--}}
{{--                                </div>--}}
{{--                            @endif--}}
{{--                        @if($extension->emergency)--}}
{{--                            <div class="col-12 col-md-3">--}}
{{--                                <p><i class="fas fa-check text-green"></i>  Contacto 24/7 para casos de emergencia--}}
{{--                                </p>--}}
{{--                            </div>--}}
{{--                        @endif--}}
{{--                            @if($extension->local_contact)--}}
{{--                                <div class="col-12 col-md-3">--}}
{{--                                    <p><i class="fas fa-check text-green"></i>  Contacto Local--}}
{{--                                    </p>--}}
{{--                                </div>--}}
{{--                            @endif--}}
{{--                        @if($extension->assistenace)--}}
{{--                            <div class="col-12 col-md-3">--}}
{{--                                <p><i class="fas fa-check text-green"></i>  Porta etiquetas--}}
{{--                                </p>--}}
{{--                            </div>--}}
{{--                        @endif--}}
{{--                            @if($extension->folders)--}}
{{--                                <div class="col-12 col-md-3">--}}
{{--                                    <p><i class="fas fa-check text-green"></i>  Carpeta de documentos--}}
{{--                                    </p>--}}
{{--                                </div>--}}
{{--                            @endif--}}
{{--                        @if($extension->destination)--}}
{{--                            <div class="col-12 col-md-3">--}}
{{--                                <p><i class="fas fa-check text-green"></i>  Informacion del destino </p>--}}
{{--                            </div>--}}
{{--                        @endif--}}
{{--                            @if($extension->itinerary)--}}
{{--                                <div class="col-12 col-md-3">--}}
{{--                                    <p><i class="fas fa-check text-green"></i>  Itinerario </p>--}}
{{--                                </div>--}}
{{--                            @endif--}}
{{--                        @if($extension->exhibitor_tips)--}}
{{--                            <div class="col-12 col-md-3">--}}
{{--                                <p><i class="fas fa-check text-green"></i>  Consejos para expositores de ferias </p>--}}
{{--                            </div>--}}
{{--                        @endif--}}
{{--                            @if($extension->tips)--}}
{{--                                <div class="col-12 col-md-3">--}}
{{--                                    <p><i class="fas fa-check text-green"></i> Consejos para visitantes de ferias</p>--}}
{{--                                </div>--}}
{{--                            @endif--}}
{{--                    </div>--}}
{{--                    @endif--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    @endif--}}
{{--    <p class="title-card font-bold mt-3">@lang('extensions.show.leagues')</p>--}}

{{--    <ul class="border p-3  mb-3">--}}
{{--        @forelse($extension->leagues->sortby('name') as $key => $league)--}}
{{--            <li>--}}
{{--                <a href="{{ $league->url }}" class="link-league">--}}
{{--                    {{ $league->name }}--}}
{{--                </a>--}}
{{--            </li>--}}
{{--        @empty--}}
{{--            <p>No tiene ninguna liga asociada</p>--}}
{{--        @endforelse--}}
{{--    </ul>--}}
    <div class="row my-4">
        <div class="col-12">
            @component('public.main.components.quotation')
                @slot('page', $extension->type)
                @slot('reference', $extension->id)
            @endcomponent
        </div>
    </div>
    @if(App::islocale('es'))
        <iframe src="https://www.facebook.com/plugins/like.php?locale=es_LA&href={{ config('app.url') }}%2F{{$url}}%2F{{$extension->slug}}%2F&width=450&layout=standard&action=like&size=small&share=false&height=35&appId=463005901528543" width="450" height="35" style="border:none;overflow:hidden;" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
        <iframe src="https://www.facebook.com/plugins/share_button.php?locale=es_LA&href={{ config('app.url') }}%2F{{$url}}%2F{{$extension->slug}}%2F&layout=button_count&size=small&appId=463005901528543&width=131&height=20" width="131" height="35" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
    @endif
    @if(App::islocale('en'))
        <iframe src="https://www.facebook.com/plugins/like.php?locale=en_US&href={{ config('app.url') }}%2F{{$url}}%2F{{$extension->slug}}%2F&width=450&layout=standard&action=like&size=small&share=false&height=35&appId=463005901528543" width="450" height="35" style="border:none;overflow:hidden;" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
        <iframe src="https://www.facebook.com/plugins/share_button.php?locale=en_US&href={{ config('app.url') }}%2F{{$url}}%2F{{$extension->slug}}%2F&layout=button_count&size=small&appId=463005901528543&width=131&height=20" width="131" height="35" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
    @endif
</div>
</div>
</div>
@endsection
@if($extension->type == 'mexico')
    @section('telephone', '525554530398')
@endif
