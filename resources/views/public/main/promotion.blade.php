@extends('public.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 mt-2">
                <h1 class="text-success titles">{{$promotion->name}}</h1>
                @if($promotion->image)
                    <a data-fancybox="gallery" href="{{ $promotion->image }}">
                        <img src="{{asset($promotion->image)}}" class="img-fluid w-25 image" alt="{{$promotion->image}}" id="logo">
                    </a>
                @else
                    <img src="{{asset('images/missing.png')}}" class="img-fluid w-25 image" alt="Sin imagen" id="poster">
                @endif
                @if(!empty($promotion->content))
                    <p class="title-card font-bold">@lang('fairs.show.description')</p>
                    <p>{!! $promotion->content !!}</p>
                @endif
                <div class="container mt-3">
                    <div class="row">
                        @foreach($images as $keyg => $gallery)
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 mt-4">
                                <a data-fancybox="gallery" href="{{ $gallery->name }}">
                                    <img src="{{asset($gallery->name)}}" class="img-fluid imagae" alt="{{$gallery->name}}" id="Imagen de galeria {{ $keyg + 1 }}">
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
