@extends('public.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-capitalize text-green font-weight-bold titles">@lang('fairs.title')</h1>
                <p>
                    <a class="nav-link text-info text-justify" data-bs-toggle="collapse" href="#collapseFairs" role="button" aria-expanded="false" aria-controls="collapseFairs">
                        <i class="fa fa-plus-circle text-info"></i>
                        @lang('fairs.description')
                    </a>
                </p>
                <div class="collapse" id="collapseFairs">
                    <div class="card card-body">
                        <ol>
                            <li>@lang('fairs.collapse.one')</li>
                            <li>@lang('fairs.collapse.two')</li>
                            <li>@lang('fairs.collapse.three')</li>
                            <li>@lang('fairs.collapse.four')</li>
                            <li>@lang('fairs.collapse.five')</li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <form class="group-form py-4">
                            <input type="text" class="form-control mb-3" placeholder="{{ trans('fairs.search') }}" value="{{ $stringSelected }}" id="search" onkeyup="change(event, this.value)">
                            <select name="sector" id="sector" class="form-control" onchange="change(event, this.value)">
                                <option value="">@lang('fairs.select')</option>
                                @foreach($sectors as $sector)
                                    <option value="{{$sector->id}}" {{ $sectorSelected == $sector->id ? 'selected':''}}>{{$sector->name}}</option>
                                @endforeach
                            </select>
                        </form>
                    </div>
                </div>
                <div class="row mb-5" id="result">
                    <div class="col-12 mb-3">
                        {{ $fairs->links() }}

                        @foreach($fairs as $fair)
                            <div class="row border-bottom border-success">
                                <div class="col-12 col-lg-3 py-3">
                                    <a href="{{ route('event', ['slug' => $fair->slug] ) }}">
                                        @if($fair->logo)
                                            <img src="{{asset($fair->logo)}}" class="img-fluid w-100 image" alt="{{$fair->logo}}" id="logo">
                                        @else
                                            <img src="{{asset('images/missing.png')}}" class="img-fluid w-100 image" alt="Sin imagen" id="logo">
                                        @endif
                                    </a>
                                </div>
                                <div class="col-12 col-lg-9 py-3">
                                    <h4 class="title-card titles">{{ $fair->name }}</h4>
                                    <p>
                                        <span class="title-card">@lang('fairs.date')</span>
                                        {{ \Carbon\Carbon::parse($fair->startDate)->translatedFormat('d F Y') }} a
                                        {{ \Carbon\Carbon::parse($fair->endDate)->translatedFormat('d F Y') }} <br>
                                        <span class="title-card">@lang('fairs.place')</span>
                                        {{$fair->place}} <br>
                                        <span class="title-card">@lang('fairs.sector')</span>
                                        {{$fair->sector->name}}
                                    </p>
                                    <p class="text-left img-fluid w-100 image">{!! $fair->short_description !!}</p>
                                    <a href="{{ route('event', ['slug' => $fair->slug] ) }}" class="btn btn-primary align-right">@lang('fairs.more')</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    {{ $fairs->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script>
        function change(event, param){
            event.preventDefault();
            if(("type" in event && event.type == "change") || ("keyCode" in event && event.keyCode == 13)){
                location.href = '/eventos/busqueda/' + param
            }
        }
    </script>
@endsection
