@extends('public.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 ">
                <h1 class="text-capitalize text-success font-weight-bold titles mb-5">@lang('news.testimonials.title')</h1>

                <div class="row">
                    <div class="col-12">
                        <p class="titlesT strongText text-left">@lang('testimonials.component.title12') </p>
                        <p class="">@lang('testimonials.component.content12')</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <p class="titlesT strongText text-left">@lang('testimonials.component.title10')</p>
                        <p class="">@lang('testimonials.component.content10')</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <p class="titlesT strongText text-left">@lang('testimonials.component.title11')</p>
                        <p class="">@lang('testimonials.component.content11')</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <p class="titlesT strongText text-left">@lang('testimonials.component.title9')</p>
                        <p class="">@lang('testimonials.component.content9')</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <p class="titlesT strongText text-left">@lang('testimonials.component.title5')</p>
                        <p class="">@lang('testimonials.component.content5')</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <p class="titlesT strongText text-left">@lang('testimonials.component.title4')</p>
                        <p class="">@lang('testimonials.component.content4')</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <p class="titlesT strongText text-left">@lang('testimonials.component.title3')</p>
                        <p class="">@lang('testimonials.component.content3')</p>
                    </div>
                </div>


                <div class="row">
                    <div class="col-12">
                        <p class="titlesT strongText text-left">@lang('testimonials.component.title8')</p>
                        <p class="">@lang('testimonials.component.content8')</p>
                    </div>
                </div>



                <div class="row">
                    <div class="col-12">
                        <p class="titlesT strongText text-left">@lang('testimonials.component.title2')</p>
                        <p class="">@lang('testimonials.component.content2')</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <p class="titlesT strongText text-left">@lang('testimonials.component.title7')</p>
                        <p class="">@lang('testimonials.component.content7')</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <p class="titlesT strongText text-left">@lang('testimonials.component.title6')</p>
                        <p class="">@lang('testimonials.component.content6')</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <p class="titlesT strongText text-left">@lang('testimonials.component.title1')</p>
                        <p class="">@lang('testimonials.component.content1')</p>
                    </div>
                </div>




            </div>
        </div>
    </div>
@endsection
@section('javascript')
@endsection
