<?php

return [
    'title' => 'Ofertas',
    'slogan' => 'Reserva hoy, viaja después',
    'search' => 'Busqueda por Nombre',
    'select' => 'Buscar por Región',
    'region' => 'Region',
    'destination' => 'Destino',
    'category' => 'Categoria',
    'more' => 'Más Información',
    'mexico' => [
        'title' => 'México',
        'slogan' => 'Sueña!, la aventura espera',
        'select' => 'Buscar por zona turística',
    ],
    'special_trips' => [
        'title' => 'Viajes Especiales',
        'slogan' => 'Reserva hoy, viaja después',
        'select' => 'Buscar por region',
    ],
    'show' => [
        'date' => 'Fecha:',
        'category' => 'Categoria:',
        'region' => 'Region:',
        'destination' => 'Destino:',
        'description' => 'Descripción:',
        'place' => 'Ubicación:',
        'leagues' => 'Ligas',
        'specialTrips' => [
            'title' => 'Viajes Especiales',
            'empty' => 'Proximamente aquí encontraras viajes especiales relacionadas con esta extensión',
        ],
    ],
];
