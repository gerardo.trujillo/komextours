<?php

return [
    'street' => 'Calle:',
    'suburb' => 'Col.',
    'side' => 'Lada sin costo:',
    'telephone' => 'Tel (Conmutador)',
    'emergencies' => 'Emergencias',
    'follow' => 'Siguenos en:',
    'copyright' => 'Todos los derechos reservados.',
    'privacy' => 'Aviso de Privacidad',
    'update' => 'Ultima Actualización',
    'horario' => 'Lunes a Viernes 09:30-14:00 y 16-19:00 horas <br> y sujeto a citas confirmadas.'
];
