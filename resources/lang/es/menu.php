<?php

return [


    'about' => 'nosotros',
    'services' => 'servicios',
    'fairs' => 'ferias',
    'extension' => 'extensiones',
    'mexico' => 'méxico',
    'specials' => 'viajes especiales',
    'contact' => 'contacto',

];
