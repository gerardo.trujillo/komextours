<?php

return [
    'title' => 'Contacto',
    'slogan' => '',
    'mailboxes' => 'Buzones de correo',
    'telephoneswitch' => 'Telefono conmutador',
    'admin' => 'Administración',
    'lada' => 'Lada sin costo:',
    'fairs' => 'Ferias Internacionales:',
    'incoming' => 'Incoming Mexico/DMC:',
    'whatsapp' => 'Emergencias fuera del horario de oficina y WhatsApp',
    'email' => 'Correo electrónico:',
    'general' => 'KOMEX tours general:',
    'fairsandextensions' => 'Ferias y extensiones:',
    'specialtrips' => 'Viajes especiales:',
    'passion' => 'Passion for Mexico:',
    'destination' => 'Destination Management:',
    'conven' => 'Convenciones & Incentivos:',
    'locate' => 'Dirección:',
    'form' => [
        'name' => 'Nombre',
        'telephone' => 'Telefono',
        'email' => 'Correo',
        'message' => 'Mensaje',
    ],
];
