<?php

return [
    'request' => 'Solicitar Cotización',
    'industry' => 'Ramo',
    'date' => 'Fecha',
    'description' => 'Descripción',
    'services' => 'Servicios',
    'location' => 'Ubicación',
    'notes' => 'Notas',
    'persons' => 'No de Personas',
    'duration' => 'Duración',
    'days' => 'Dias',
    'visit' => 'Visitante / Expositor',
    'company' => 'Compañia',
    'telephone' => 'Telefono',
    'email' => 'Correo',
    'name' => 'Nombre Completo',
];
