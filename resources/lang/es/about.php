<?php

return [


    'title' => 'Quienes Somos',
    'history' => [
        'title' => 'Historia',
        'paragraph1' => 'KOMEX tours, SA de CV es una agencia operadora/mayorista de viajes oficialmente registrada con antecedentes desde 1968, establecida como empresa independiente en 1990 en México, DF. y asociada con el Grupo ROYALE desde 2003. La compañía es socio de la Cámara Nacional de Comercio (CANACO), de la Cámara Mexicano-Alemana de Comercio e Industria (CAMEXA) y de la Asociación Mexicana de Agentes de Viajes en la Cd. México (AMAV CDMX).',
        'paragraph2' => 'KOMEX tours agencia de viajes oficial de la feria ITMA en México. Asimismo, cooperamos estrechamente con DE Internacional (representante oficial de los recintos feriales de Colonia, Munich, Berlín, Nuremberg), las ferias de Frankfurt y Hannover en la CDMX.'
    ],
    'office' => [
        'title' => 'Nuestras Oficinas',
        'paragraph1' => 'La Pandemia del Coronavirus cambió al mundo, en particular al turismo global como lo conocíamos antes. ',
        'paragraph2' => ' Muchas empresas cerraron o están luchando para su sobrevivencia, tanto navieras de cruceros, compañías aéreas como hoteles, operadoras y agencias de viajes.  A pesar o precisamente por esta razón, las agencias de viajes han comprobado su valor particular para los clientes en momentos de crisis en comparación con las ventas en línea.',
        'paragraph3' => 'Para seguir adelante, el reto es reinventar el negocio tradicional y adaptarse a los cambios por venir. En KOMEX tours lo hemos tomado muy en serio, y convertimos nuestra agencia en una “Boutique Virtual de Asesoría en Viajes” para clientes nacionales. – ¿Qué significa esto?',
        'paragraph4' => 'Boutique indica que la gama de servicios en viajes es muy selecta, de alta calidad y confiable, segura, a la medida y a precios justos, nacional como internacional. Nos enfocamos a un servicio personalizado, tanto para empresas como clientes privados en México. Nuestra prioridad es el servicio al cliente en todos sus aspectos.',
        'paragraph5' => 'Por otro lado, la limitación de una boutique significa también que no somos una agencia para todo tipo de viajes, o sea, no ofrecemos viajes de turismo masivo ni queremos competir con el precio más bajo del mercado (plataformas de internet) o de bajo presupuesto. No somos agentes de ventas. Somos asesores de viaje profesionales con más de 30 años de experiencia.',
        'paragraph6' => 'Trabajamos desde casa y asesoramos a nuestros clientes en línea por teléfono como antes. ',
        'paragraph7' => 'Y lo nuevo es que, concertamos citas de consulta virtuales por medio de Skype, Zoom o similar con todo el apoyo tecnológico moderno. Gracias a la tecnología podemos mostrarles en línea documentos, presentaciones, videos, etc. sobre destinos, hoteles, atracciones en todo el mundo e itinerarios para diseñar viajes a la medida.',
        'paragraph8' => 'Dentro de la CDMX ofrecemos también un servicio de asesoría a domicilio. Nuestros asesores visitan a los clientes en su hogar a solicitud.',
        'paragraph9' => 'Contamos con alta tecnología de soporte como sistema de reservaciones en línea.',
        'paragraph10' => 'Nuestra administración se encuentra bajo la dirección publicada, a cargo de la facturación y el manejo de pagos en línea.',
        'paragraph11' => 'Tenemos visión, misión y valores. En resumen: El cliente primero.',
    ],
    'colaborators' => [
        'title' => 'Nuestros Colaboradores',
        'card1' => [
            'name' => 'Suzanne C. Holst',
            'title' => 'Gerente de Ventas y Ferias Internacionales',
            'paragraph1' => 'Tiene más de 25 años en el turismo de exportación y es especialista en Europa, Oriente y Sudamérica. Radicó y trabajó en Uruguay y 9 años en Alemania, y habla tanto alemán, como francés e inglés. Su pasatiempo preferido es viajar por el mundo, lo que explica su afán de organizar viajes muy especiales como a la India (2011 y 2012 y 2014) o Indochina (2013 y 2015),Kenia y Tanzania (2014), Portugal y Marruecos (2015), Rusia y Escandinavia (2016), Sudáfrica y Namibia (2017).',
            'paragraph2' => 'Países visitados: Alemania, Francia, Rep. Checa, Austria, Inglaterra, Italia, España, Portugal, Grecia, Turquía, Jordania, Egipto, Marruecos, India, Nepal, Tailandia, Vietnam, Camboya, Hong Kong, Corea del Sur, Japón, Australia, Nueva Zelandia, Singapur, Filipinas, Kenia, Tanzania, Estados Unidos.'
        ],
        'card2' => [
            'name' => 'Sr. Hans-Peter Holst',
            'title' => 'Dirección General, Ventas Internacionales',
            'paragraph1' => 'El director de la empresa es profesional en turismo desde 1974 con una amplia experiencia en la operación de viajes tanto en México como en Europa (su país natal: Alemania) y las Américas. Habla tanto Español como Inglés y Francés. Especialista en organización de Convenciones y Grupos especiales, conoce México como destino turístico como su propio país, y lo promociona constantemente en importantes Ferias del mundo, representando al Grupo ROYALE.',
            'paragraph2' => 'Países visitados: Suecia, Dinamarca, Noruega, Islandia, Alemania, Holanda, Bélgica, Francia, Rep. Checa, Austria, Suiza, Inglaterra, Portugal, España, Italia, Grecia, Turquía, Estados Unidos, Canadá, Brasil, Uruguay, Argentina, Chile, Perú, Venezuela, Rep. Dominicana, Cuba, Guatemala entre otros y desde luego muchas partes de México entre Tijuana y Cozumel.'
        ],
        'card3' => [
            'name' => 'Lic. Lucero Flores V.',
            'title' => 'Asistente de Mercadotecnia/Internet',
            'paragraph1' => ' Licenciada en Administración del Tiempo Libre con especialidad en Turismo. Es apasionada del diseño y de la mercadotecnia y Asistente de Mercadotecnia desde 2016. Además es el enlace del Grupo ROYALE y coordinadora de las actividades internas del grupo.',
            'paragraph2' => ''
        ],
        'final' => 'Nuestros Representantes internacionales de Ventas',
    ],
    'competittions' => [
        'title' => 'Nuestra Competencia',
        'paragraph' => [
            '1' => 'Viajes de Negocios a Ferias Internacionales y extensiones para Expositores y Visitantes',
            '2' => 'Cruceros y Circuitos culturales',
            '3' => 'Viajes especiales para grupos en todo el mundo',
            
            '4' => 'Congresos, Convenciones y Juntas de negocios',
            '5' => 'Turismo receptivo y Gerenciamiento de Destino (DMC) en México)',
            '6' => 'Servicio Concierge (Gestión de Estilo de Vida y Asistencia Personal)',
        ]
    ],
    'royale' => [
        'title' => 'Grupo ROYALE',
        'paragraph1' => 'El Grupo ROYALE es un grupo mexicano con actividades en el ramo turístico y cuenta con oficinas en Cancún, Playa del Carmen/Riviera Maya, Cozumel, Chetumal/Costa Maya, Mérida/Yucatán, Huatulco, Ciudad de México, Acapulco, Puerto Vallarta, Guadalajara, Los Cabos/Baja California, Monterrey y actualmente alrededor de 150 empleados. En los destinos de playa nos dedicamos esencialmente al "Destination Management" (servicios en el destino) y en CDMX al Turismo Receptivo (operadora/mayorista de circuitos y tours en todo México), conocido principalmente en el medio turístico.',
        'paragraph2' => 'Entre otros, las oficinas del Grupo prestan todos los servicios de turismo y de apoyo logístico para operadores de turismo, clientes corporativos y asociaciones nacionales e internacionales. Actualmente el Grupo ROYALE está implementando el nuevo “Servicio ROYALE Concierge” para ofrecer al viajero un servicio único en el destino (gestión de Estilo de Vida y Asistencia Personal).',
        'paragraph3' => 'KOMEX tours/Grupo ROYALE promociona el destino México en varias ferias internacionales p.ej. WTM World Travel Market de Londres, la ITB Internationale Tourismus Börse de Berlin, IMEX de Frankfurt, IT&ME de Chicago o SEATRADE de Miami. Como compradores visitamos también ferias como el Tianguis Turístico de México y IBTM de las Américas entre otros.',
    ],
    'questions' => [
        'title' => 'Preguntas Frecuentes',
        'points' =>  [
            '1' => [
                'title' => '¿Todas las Agencias de Viajes son iguales?',
                'paragraph1' => 'De hecho, cada Agencia de Viajes es diferente y tiene diferentes campos de especialización. Dependiendo del tipo de viaje que está buscando, una agencia le conviene más que otra.

'
            ],
            '2' => [
                'title' => '¿Es correcto afirmar que nadie usa Agencias de Viajes hoy en día?',
                'paragraph1' => 'Las agencias viajes venden actualmente todavía 51% de los boletos aéreos, 87% de los cruceros, 45% de las rentas de auto y aprox. 47% de todos los hoteles según la ASTA (Sociedad Americana de Agencias de Viajes).'
            ],
            '3' => [
                'title' => '¿Las Agencias de Viajes han perdido su justificación de existir?',
                'paragraph1' => 'Agentes de Viajes reservan hoteles, cruceros y actividades todos los días. Los proveedores de estos servicios lo saben y están más dispuestos a conceder excepciones para las Agencias de Viajes.'
            ],
            '4' => [
                'title' => '¿La necesidad de usar Agentes de Viajes no está en proceso de substitución por el Internet?',
                'paragraph1' => 'Existen algunas cosas que la tecnología no puede replicar y el toque personal es una de ellas. El Internet es una fuente valiosa, pero no puede substituir la experiencia, la dirección y el servicio personal de un agente de viajes. Cuando los viajeros están estresados por agendas apretadas, los agentes de viajes tienen toda la información a la mano, ahorrándole horas valiosas en el Internet. Los agentes pueden ofrecer también recomendaciones usualmente basadas en propias experiencias.'
            ],
            '5' => [
                'title' => '¿Las Agencias de Viajes tienen la información actualizada como el Internet?',
                'paragraph1' => 'Las Agencias de Viajes obtienen algunas de sus informaciones de las mismas fuentes como los sitios de OTAs (agencias de viajes en línea). Reciben también diariamente mails y avisos sobre las últimas ofertas especiales de resorts y hoteles que no siempre se publican por Internet. Así como también, pueden llamar a un lugar directamente para verificar si pueden conseguir plus especial para usted, lo que un sitio en línea no puede hacer.'
            ],
            '6' => [
                'title' => '¿Las Agencias de Viajes sólo pueden reservar mi vuelo y mi hotel?',
                'paragraph1' => 'Es un hecho que agentes de viajes pueden arreglar servicios de autos, paseos personalizados, actividades y seguros; adicionalmente a todos los servicios básicos de viajar. Son también una fuente excelente de información referente a restaurantes recomendables, sitios para visitar y consejos al empacar.'
            ],
            '7' => [
                'title' => '¿Cómo se distingue KOMEX tours de otras agencias?',
                'paragraph1' => 'Estamos orgullos de ofrecer a nuestros clientes un servicio amplio de alta calidad con una atención personalizada con más de 30 años de experiencia en viajes por todo el mundo. Estamos registrados en el Registro Nacional de Turismo (No. 04090141369) y tenemos la certificación Distintivo M. Nuestras especialidades son viajes a la medida para expositores y visitantes de ferias internacionales en los 5 continentes, extensiones individuales, viajes particulares para grupos y de lujo; así como servicios turísticos en México y el Mundo Maya (turismo receptivo), como son viajes individuales y grupos, circuitos culturales, incentivos y convenciones. KOMEX tours es una empresa del Grupo ROYALE con socios en los principales destinos de México, quienes brindan personalmente el servicio de receptivo al cliente.',
                'paragraph2' => 'Yo puedo reservar fácilmente mi propio viaje sin usar una Agencia de Viajes ¿Cuál es la diferencia? Las Agencias de Viajes además de tener acceso a todos los sitios que usted usaría en su búsqueda de reservar algún viaje, también tienen acceso a cotizaciones y acuerdos especiales para paquetes, que frecuentemente no son disponibles al público. Su Agente de Viajes tiene más oportunidades de ayudarle en algunas situaciones, como por ejemplo: cuando encuentra hoteles que pretenden estar totalmente ocupados, para reservaciones por Agencias pueden tener habitaciones vacantes. Así, si alguien le dice “No” su Agente de Viajes puede ayudar en convertirlo en un “Sí”.'
            ],
            '8' => [
                'title' => '¿No es más caro usar una Agencia de Viajes?',
                'paragraph1' => 'Los cargos por servicio dependen realmente de cada Agencia. Mientras algunas Agencias más lujosas tienes cargos altos, el cargo promedio esta relativamente al margen. Algunas agencias están dispuestas a omitir u ofrecer un descuento una vez que haya finalizado el viaje con ellas. Además, puede preguntar antes cuánto le cobran y decidir si vale la pena. También es importante recordar que muchas páginas Web en línea como. Expedia, Best Day, Price Travel y muchos otros proveedores cobran cargos por servicio.'
            ],
            '9' => [
                'title' => '¿Las Agencias de Viajes abusan cuándo no me ofrecen el precio más barato?',
                'paragraph1' => 'Los Agentes de Viajes conocen las ventajas y desventajas de los diferentes itinerarios. Usted puede encontrar alguno más económico, pero esta diferencia podría causarle muchos trastornos. Por ejemplo, estancias más largas forzadas en el aeropuerto y/o horarios de viaje inconvenientes. Un buen Agente de Viajes intentará darle el mayor valor a su dinero, lo que incluye las conexiones más directas e itinerarios más convenientes que puede encontrar'
            ],
            '10' => [
                'title' => '¿No es una pérdida de tiempo de consultar una Agencia de Viajes?',
                'paragraph1' => 'A pesar de que puede encontrar por usted mismo mucha de la información que el Agente de Viajes le provee, ahorraría mucho de su valioso tiempo. Un Agente de Viajes le ahorra horas de búsquedas pesadas y comparativos de precios de compra. Cuenta con tarifas actualizadas, condiciones de hoteles e interesantes actividades, además de que su conocimiento y experiencia le dan la ventaja en la planeación de viajes.'
            ],
            '11' => [
                'title' => '¿Por qué debería usar una Agencia de Viajes?',
                'paragraph1' => 'La Agencia de Viajes tiene muchas ventajas para el viajero:',
                'paragraph2' => 'ABOGADO DEL CLIENTE: Si tiene un problema en particular dentro de su viaje, el Agente está de su lado para actuar en su representación y conseguir una restitución.',
                'paragraph3' => 'CONSEJOS PROFESIONALES: Los Agentes de Viajes existen para asegurar lo que obtiene, dónde y cuándo quiere viajar y el precio más favorable.',
                'paragraph4' => 'DIRECCIÓN DE UN EXPERTO: Los Agentes de Viajes son expertos en la comprensión de informaciones de viajes y códigos',
                'paragraph5' => 'INFORMACIÓN IMPARCIAL: Los Agentes trabajan para sus clientes y no para sus proveedores. Y saben que un cliente feliz será un cliente repetitivo.',
                'paragraph6' => 'SEGURIDAD: Una Agencia de Viajes está registrada y sujeta a las leyes del país, tanto de protección al consumidor como fiscal. Las leyes en México son diferentes y muy distintas para agencias/operadores de viajes, transportistas y hoteles.',
                'paragraph7' => 'SERVICIO PERSONALIZADO: En vez de una voz impersonal a miles de kilómetros de distancia, los Agentes de Viajes son sus “vecinos” y saben lo que busca y valora en sus experiencias de viaje.',
                'paragraph8' => 'TIEMPO: En vez de verificar una lista larga de páginas Web de viajes, que sólo ofrecen tarifas y precios para las empresas que tienen contratos con ellos, ¿por qué no ir directamente a la fuente original? Un agente de viajes tiene toda la información a mano, ahorrándole horas frente al monitor de su computadora.',
            ],
            '12' => [
                'title' => '¿Qué beneficios me da KOMEX tours como cliente?',
                'paragraph1' => 'En KOMEX tours estamos comprometidos con nuestros clientes y no con algún proveedor en particular. Con honestidad y transparencia asesoramos, coordinamos, organizamos y reservamos todos los detalles del viaje. Nos ponemos la camiseta del cliente y tratamos su viaje como sería un tour para nosotros mismos. Nuestros servicios incluyen tanto el servicio pre y post venta, así como la ayuda 24/7 en casos de emergencia durante su viaje y la asistencia en caso de cambios y cancelaciones; y naturalmente precios finales y facturación fiscal completa. Nuestra responsabilidad y condiciones claras, garantizan amistades largas. Deseamos que nuestros clientes queden satisfechos, vuelvan y nos recomienden.'
            ],
            '13' => [
                'title' => '¿Cuáles son los horarios de oficina de KOMEX tours?',
                'paragraph1' => 'Trabajamos de Lunes a Viernes de 08:30 a 19:00 hrs, y Sábado de 09:00 a 13:00 hrs. En caso de emergencia nuestra Gerencia se encuentra al pendiente las 24 hrs. del día.'
            ],
        ]
    ],
    'best' => [
        'title' => 'Mejores Prácticas',
         'points' => [
             '1' => [
                 'paragraph1' => 'Procedimientos generales',
                 'paragraph2' => 'Pagos con tarjeta de crédito o por transferencia en MXN y USD',
                 'paragraph3' => 'Seguros de Asistencia de Viajes',
                 'paragraph4' => 'Familiarización con las condiciones de nuestros proveedores',
                 'paragraph5' => 'Información actualizada de fuentes oficiales',
                 'paragraph6' => 'Documentación de comunicación',
                 'paragraph7' => 'Políticas flexibles de Cancelación',
                 'paragraph8' => 'Servicio telefónico con atención personalizada',
                 'paragraph9' => 'Horarios extendidos del negocio',
                 'paragraph10' => 'Servicio de Emergencia 24/7',
                 'paragraph11' => 'Cumplimos nuestras promesas',
                 'paragraph12' => 'Asistencia y Servicio de Soporte en el destino',
             ],
             '2' => [
                'title' => 'Valores Agregados',
                 'paragraph1' => 'Precios finales y garantizados (sin cargos extras escondidos)',
                 'paragraph2' => 'Recomendaciones útiles para viajeros',
                 'paragraph3' => 'Seguros de viajes',
                 'paragraph4' => 'Destino',
                 'paragraph5' => 'Extensiones',
                 'paragraph6' => 'Consejos para Visitantes y Expositores de Ferias internacionales',
                 'paragraph7' => 'Viajar de manera sostenible',
                 'paragraph8' => 'Guía para Eventos sostenibles',
                 'paragraph9' => 'Facturación fiscal',
                 'paragraph10' => 'Servicio de Mensajería',
                 'paragraph11' => 'Asistencia en cambios, quejas y trámites de reembolso',
             ]
         ]
    ],

];
