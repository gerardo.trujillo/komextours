<?php

return [
    'street' => 'Street:',
    'suburb' => 'Suburb.',
    'side' => 'Free side:',
    'telephone' => 'Tel (switch)',
    'emergencies' => 'Emergencies',
    'follow' => 'Follow us on:',
    'copyright' => 'All rights reserved.',
    'privacy' => 'Privacy Notice',
    'update' => 'Last Update',
    'horario' => 'Monday through Friday 09:30-14:00 y 16-19:00 horas <br> subjet to  confirmed appointments.'
];
