<?php

return [
    'title' => 'Offers',
    'slogan' => 'Book today, travel later',
    'search' => 'Search by Name',
    'select' => 'Search by Region',
    'region' => 'Region',
    'destination' => 'Destination',
    'category' => 'Category',
    'more' => 'More Information',
    'mexico' => [
        'title' => 'Mexico',
        'slogan' => 'Dream, adventure awaits',
        'select' => 'Search by tourist area',
    ],
    'special_trips' => [
        'title' => 'Special Trips',
        'slogan' => 'Book today, travel later',
        'select' => 'Search by region',
    ],
    'show' => [
        'date' => 'Date:',
        'category' => 'Category:',
        'region' => 'Region:',
        'destination' => 'Destination:',
        'description' => 'Description:',
        'place' => 'Location:',
        'leagues' => 'Leagues',
        'specialTrips' => [
            'title' => 'Special Trips',
            'empty' => 'Coming soon here you will find special trips related to this extension',
        ],
    ],
];
