<?php

return [
    'more' => 'See more',
    'fairs' => [
        'title' => 'International Trade Fairs',
        'content' => 'Consultation and special arrangement facilities for exhibitors and visitors of international fairs',
    ],
    'extensions' => [
        'title' => 'Extensions',
        'content' => 'Depending on the location of a trade fair we offer tailor-made extensions',
    ],
    'tours' => [
        'title' => 'Tailor-made tours',
        'content' => 'We organize tailor-made group tours to national and international destinations',
    ],
    'conve' => [
        'title' => 'Conventions',
        'content' => 'Consultation and special arrangement facilities for exhibitors and visitors to international trade fairs',
    ],
    'fairsnext' => [
        'title' => 'Upcoming International Trade Fairs',
        'star' => 'Home',
        'place' => 'Location',
    ]
];
