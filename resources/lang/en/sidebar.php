<?php

return [

    'group' => 'Royale Group',
    'news' => 'News',
    'blog' => 'Blog',
    'offers' => 'Offers',
    'testimonials' => 'Testimonials',
    'global' => 'Global Conditions',
    'service' => 'Service Menu',

];
