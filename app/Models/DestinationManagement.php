<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DestinationManagement extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'destination_management';
    protected $fillable = ['url', 'image'];
}
