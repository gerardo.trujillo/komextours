<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Sector extends Model
{
    use HasFactory, SoftDeletes, HasTranslations;

    protected $table = "sectors";

    protected $translatable = ['name'];

    protected $fillable = ['name', 'active'];
}
