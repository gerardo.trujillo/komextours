<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Destination extends Model
{
    use HasFactory, HasTranslations;

    protected $table = "destinations";

    protected $translatable = ['destination'];
}
