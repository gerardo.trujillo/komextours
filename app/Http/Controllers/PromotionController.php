<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Models\Promotion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class PromotionController extends Controller
{
    protected $title;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->title = 'Promociones';
        View::share('title', $this->title);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promotions = Promotion::get();
        return view('administrador.promotions.list', compact('promotions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrador.promotions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->file('image');
        $data = $request->validate([
            'name' => 'required|string|min:5',
            'image' => 'required'
        ],[
            'name.required' => 'El campo nombre es obligatorio',
            'name.string' => 'El campo nombre debe ser una cadena de texto',
            'name.min' => 'El campo nombre debe tener como minimo 5 caracteres',
            'image.required' => 'El campo imagen es obligatorio',
        ]);
        $promotion = new Promotion();
        $promotion->name = $data['name'];
        $promotion->content = $request['content'];
        $promotion->url = $request['url'];
        if($image) {
            $nameImage = time() . "_" . $image->getClientOriginalName();
            Storage::disk('public')->put('/images/promotions/' . $nameImage, \File::get($image));
            $promotion->image = '/storage/images/promotions/'.$nameImage;
        }
        $promotion->save();
        return redirect('admin/promotions')->with('success', 'La promoción se creo con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function show(Promotion $promotion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function edit(Promotion $promotion)
    {
        return view('administrador.promotions.edit', compact('promotion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Promotion $promotion)
    {
        $image = $request->file('image');
        $data = $request->validate([
            'name' => 'required|string|min:5'
        ],[
            'name.required' => 'El campo nombre es obligatorio',
            'name.string' => 'El campo nombre debe ser una cadena de texto',
            'name.min' => 'El campo nombre debe tener como minimo 5 caracteres'
        ]);
        $promotion->name = $data['name'];
        $promotion->content = $request['content'];
        $promotion->url = $request['url'];
        if($image) {
            $nameImage = time() . "_" . $image->getClientOriginalName();
            Storage::disk('public')->put('/images/promotions/' . $nameImage, \File::get($image));
            $promotion->image = '/storage/images/promotions/'.$nameImage;
        }
        $promotion->save();
        return redirect('admin/promotions')->with('success', 'La promoción se actualizo con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Promotion $promotion)
    {
        $promotion->delete();
        return redirect('admin/promotions')->with('success', 'La promoción se elimino con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        $promotion = Promotion::findOrFail($id);
        if($promotion->active){
            $promotion->active = false;
            $result = 'desactivo';
        } else {
            $promotion->active = true;
            $result = 'activo';
        }
        $promotion->save();
        return redirect('admin/promotions')->with('success', 'La promoción se '. $result.' correctamente');
    }
}
