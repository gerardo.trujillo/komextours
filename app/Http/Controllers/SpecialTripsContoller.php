<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Destination;
use App\Models\Extension;
use App\Models\Image;
use App\Models\League;
use App\Models\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str as Str;

class SpecialTripsContoller extends Controller
{
    protected $title;
    protected $type;
    protected $url;
    protected $single;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->title = "Viajes Especiales";
        $this->type = "special_trips";
        $this->single = "Viaje Especial";
        $this->url = 'viajes-especiales';

        $destinations = Destination::get();
        $regions = Region::get();
        $categories = Category::get();
        $destinationsAll = Destination::get();
        $destinationsMexico = Region::where('type', 2)->get();
        $leagues = League::get();

        View::share('type', $this->type);
        View::share('url', $this->url);
        View::share('title', $this->title);
        View::share('single', $this->single);
        View::share('destinationsAll', $destinationsAll);
        View::share('destinationsMexico', $destinationsMexico);
        View::share('destinations', $destinations);
        View::share('regions', $regions);
        View::share('categories', $categories);
        View::share('leagues', $leagues);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $extensions = Extension::where('type' , 'special_trips')->get();
        return view('administrador.extensions.list', compact('extensions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('administrador.extensions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => 'es']);
        $extension = new Extension($request->all());
        $extension->slug = Str::slug($extension->name);
        $extension->region_id = $request['region'];
        $extension->destination_id = $request['destination_id'];
        $extension->category_id = $request['category'];
        $extension->type = 'special_trips';
        $itinerary_file = $request->file('itinerary_file');
        if($itinerary_file) {
            $nameItinerary_file = "pdf_".time().".".$itinerary_file->guessExtension();
            Storage::disk('public')->put('/files/special_trips/' . $nameItinerary_file, \File::get($itinerary_file));
            $extension->itinerary_file = '/storage/files/special_trips/'.$nameItinerary_file;
        }
        $extension->museum_package = $request->has('museum_package');
        $extension->pack_city = $request->has('pack_city');
        $extension->active = $request->has('active');
        $extension->itinerary = $request->has('itinerary');
        $extension->assistance = $request->has('assistance');
        $extension->trunks = $request->has('trunks');
        $extension->charges = $request->has('charges');
        $extension->taxes = $request->has('taxes');
        $extension->emergency = $request->has('emergency');
        $extension->local_contact = $request->has('local_contact');
        $extension->folders = $request->has('folders');
        $extension->labels = $request->has('labels');
        $extension->destination = $request->has('destination');
        $extension->tips = $request->has('tips');
        $extension->exhibitor_tips = $request->has('exhibitor_tips');
        $extension->save();

        config(['app.locale' => 'en']);
        $extension = new Extension($request->all());
        $extension->slug = Str::slug($extension->name);
        $extension->region_id = $request['region'];
        $extension->destination_id = $request['destination_id'];
        $extension->category_id = $request['category'];
        $extension->type = 'special_trips';
        $itinerary_file = $request->file('itinerary_file');
        if($itinerary_file) {
            $nameItinerary_file = "pdf_".time().".".$itinerary_file->guessExtension();
            Storage::disk('public')->put('/files/special_trips/' . $nameItinerary_file, \File::get($itinerary_file));
            $extension->itinerary_file = '/storage/files/special_trips/'.$nameItinerary_file;
        }
        $extension->museum_package = $request->has('museum_package');
        $extension->pack_city = $request->has('pack_city');
        $extension->active = $request->has('active');
        $extension->itinerary = $request->has('itinerary');
        $extension->assistance = $request->has('assistance');
        $extension->trunks = $request->has('trunks');
        $extension->charges = $request->has('charges');
        $extension->taxes = $request->has('taxes');
        $extension->emergency = $request->has('emergency');
        $extension->local_contact = $request->has('local_contact');
        $extension->folders = $request->has('folders');
        $extension->labels = $request->has('labels');
        $extension->destination = $request->has('destination');
        $extension->tips = $request->has('tips');
        $extension->exhibitor_tips = $request->has('exhibitor_tips');
        $extension->save();

        $extension->leagues()->sync($request['leagues']);

        return redirect('admin/trips')->with('success', 'La extensión Viajes Especiales se creo con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $extension = Extension::where('type', 'special_trips')->where('slug', 'LIKE', '%'.$slug.'%' )->first();
        if(!$extension){
            return redirect('/');
        }
        $image = Image::where('type', 'special_trips')->where('reference', $extension->id)->first();
        $images = Image::where('type', 'special_trips')->where('reference', $extension->id)->get();
        $description = strip_tags($extension->description);
        $description = html_entity_decode($description);

        return view('public.main.extension', compact('extension', 'image', 'images', 'description'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $extension = Extension::findOrFail($id);
        $destinations = Destination::where('region_id', $extension->region_id)->get();
        $images = Image::where('type', 'special_trips')->where('reference', $id)->get();
        return view('administrador.extensions.edit', compact('extension', 'images', 'destinations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => 'es']);
        $extension = Extension::findOrFail($id);
        $extension->fill($request->all());
        $extension->region_id = $request['region'];
        $extension->destination_id = $request['destination_id'];
        $extension->category_id = $request['category'];
        $itinerary_file = $request->file('itinerary_file');
        if($itinerary_file) {
            $nameItinerary_file = "pdf_".time().".".$itinerary_file->guessExtension();
            Storage::disk('public')->put('/files/special_trips/' . $nameItinerary_file, \File::get($itinerary_file));
            $extension->itinerary_file = '/storage/files/special_trips/'.$nameItinerary_file;
        }
        $extension->slug = Str::slug($extension->name);
        $extension->museum_package = $request->has('museum_package');
        $extension->pack_city = $request->has('pack_city');
        $extension->active = $request->has('active');
        $extension->itinerary = $request->has('itinerary');
        $extension->assistance = $request->has('assistance');
        $extension->trunks = $request->has('trunks');
        $extension->charges = $request->has('charges');
        $extension->taxes = $request->has('taxes');
        $extension->emergency = $request->has('emergency');
        $extension->local_contact = $request->has('local_contact');
        $extension->folders = $request->has('folders');
        $extension->labels = $request->has('labels');
        $extension->destination = $request->has('destination');
        $extension->tips = $request->has('tips');
        $extension->exhibitor_tips = $request->has('exhibitor_tips');
        $extension->save();

        config(['app.locale' => 'en']);
        $extension = Extension::findOrFail($id);
        $extension->fill($request->all());
        $extension->region_id = $request['region'];
        $extension->destination_id = $request['destination_id'];
        $extension->category_id = $request['category'];
        $itinerary_file = $request->file('itinerary_file');
        if($itinerary_file) {
            $nameItinerary_file = "pdf_".time().".".$itinerary_file->guessExtension();
            Storage::disk('public')->put('/files/special_trips/' . $nameItinerary_file, \File::get($itinerary_file));
            $extension->itinerary_file = '/storage/files/special_trips/'.$nameItinerary_file;
        }
        $extension->slug = Str::slug($extension->name);
        $extension->museum_package = $request->has('museum_package');
        $extension->pack_city = $request->has('pack_city');
        $extension->active = $request->has('active');
        $extension->itinerary = $request->has('itinerary');
        $extension->assistance = $request->has('assistance');
        $extension->trunks = $request->has('trunks');
        $extension->charges = $request->has('charges');
        $extension->taxes = $request->has('taxes');
        $extension->emergency = $request->has('emergency');
        $extension->local_contact = $request->has('local_contact');
        $extension->folders = $request->has('folders');
        $extension->labels = $request->has('labels');
        $extension->destination = $request->has('destination');
        $extension->tips = $request->has('tips');
        $extension->exhibitor_tips = $request->has('exhibitor_tips');
        $extension->save();

        $extension->leagues()->sync($request['leagues']);
        return redirect('admin/trips')->with('success', 'La extensión Viajes Especiales se actualizo con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $extension = Extension::findOrFail($id);
        $extension->delete();
        return redirect('admin/trips')->with('success', 'La extensión Viajes Especiales se elimino con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        $extension = Extension::findOrFail($id);
        if($extension->active){
            $extension->active = false;
            $result = 'desactivo';
        } else {
            $extension->active = true;
            $result = 'activo';
        }
        $extension->save();
        return redirect('admin/trips')->with('success', 'La extensión Viajes Especiales se '. $result.' correctamente');
    }
}
