<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Destination;
use App\Models\Fair;
use App\Models\Image;
use App\Models\League;
use App\Models\Offer;
use App\Models\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str as Str;

class OfferController extends Controller
{
    protected $title;
    protected $type;
    protected $single;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->title = "Ofertas";
        $this->type = "offers";
        $this->single = "Oferta";

        $destinations = Destination::get();
        $regions = Region::get();
        $categories = Category::get();
        $leagues = League::get();
        $fairs = Fair::get();

        View::share('title', $this->title);
        View::share('type', $this->type);
        View::share('single', $this->single);
        View::share('destinations', $destinations);
        View::share('regions', $regions);
        View::share('categories', $categories);
        View::share('leagues', $leagues);
        View::share('fairs', $fairs);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $extensions = Offer::get();
        return view('administrador.extensions.list', compact('extensions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrador.extensions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $extension = new Offer($request->all());
        $extension->slug = Str::slug($extension->name);
        $extension->region_id = $request['region'];
        $extension->destination_id = $request['destination_id'];
        $extension->category_id = $request['category'];
        $extension->fair_id = $request['fair'];
        $extension->active = $request->has('active');
        $extension->itinerary = $request->has('itinerary');
        $extension->assistance = $request->has('assistance');
        $extension->trunks = $request->has('trunks');
        $extension->charges = $request->has('charges');
        $extension->taxes = $request->has('taxes');
        $extension->emergency = $request->has('emergency');
        $extension->local_contact = $request->has('local_contact');
        $extension->folders = $request->has('folders');
        $extension->labels = $request->has('labels');
        $extension->destination = $request->has('destination');
        $extension->tips = $request->has('tips');
        $extension->exhibitor_tips = $request->has('exhibitor_tips');
        $extension->save();
        $extension->leagues()->sync($request['leagues']);

        return redirect('admin/offers')->with('success', 'La oferta se creo con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $offer = Offer::where('active', true)->where('slug', 'LIKE', '%'.$slug.'%')->first();
        if(!$offer){
            return redirect('/');
        }
        $image = Image::where('type', 'extension')->where('reference', $offer->id)->first();
        $images = Image::where('type', 'extension')->where('reference', $offer->id)->get();
        return view('public.main.offer', compact('offer', 'image', 'images'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $extension = Offer::findOrFail($id);
        $images = Image::where('type', 'offers')->where('reference', $id)->get();
        return view('administrador.extensions.edit', compact('extension', 'images'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $extension = Offer::findOrFail($id);
        $extension->fill($request->all());
        $extension->region_id = $request['region'];
        $extension->fair_id = $request['fair_id'];
        $extension->category_id = $request['category'];
        $extension->slug = Str::slug($extension->name);
        $extension->active = $request->has('active');
        $extension->itinerary = $request->has('itinerary');
        $extension->assistance = $request->has('assistance');
        $extension->trunks = $request->has('trunks');
        $extension->charges = $request->has('charges');
        $extension->taxes = $request->has('taxes');
        $extension->emergency = $request->has('emergency');
        $extension->local_contact = $request->has('local_contact');
        $extension->folders = $request->has('folders');
        $extension->labels = $request->has('labels');
        $extension->destination = $request->has('destination');
        $extension->tips = $request->has('tips');
        $extension->exhibitor_tips = $request->has('exhibitor_tips');
        $extension->save();

        $extension->leagues()->sync($request['leagues']);
        return redirect('admin/offers')->with('success', 'La oferta se actualizo con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $extension = Offer::findOrFail($id);
        $extension->delete();
        return redirect('admin/offers')->with('success', 'La oferta se elimino con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        $extension = Offer::findOrFail($id);
        if($extension->active){
            $extension->active = false;
            $result = 'desactivo';
        } else {
            $extension->active = true;
            $result = 'activo';
        }
        $extension->save();
        return redirect('admin/offers')->with('success', 'La oferta se '. $result.' correctamente');
    }
}
