<?php

namespace App\Http\Controllers;

use App\Models\Extension;
use App\Models\Fair;
use App\Models\Image;
use App\Models\News;
use App\Models\Offer;
use App\Models\Promotion;
use App\Models\Region;
use App\Models\Sector;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{
    public function index()
    {
        $date = date('Y-m-d');
        $fairsLast = Fair::whereDate('endDate', $date)->get();
        foreach ($fairsLast as $last){
            $last->active = false;
            $last->save();
        }
        $promotions = Promotion::where('active', true)->get();
        $fairs = Fair::where('star', true)->where('active', true)->get();
        return view('public.main.index', compact('promotions', 'fairs'));
    }

    public function noticePrivacy()
    {
        $page = 'contact';
        return view('public.main.notice-of-privacy', compact('page'));
    }

    public function about()
    {
        $page = "about";
        return view('public.main.about', compact('page'));
    }

    public function services(Request $request)
    {
        $page = 'services';
        $item = $request['item'];
        return view('public.main.services', compact('item', 'page'));
    }

    public function events()
    {
        $fairsActive = Fair::whereDate('endDate', '<', Carbon::now()->format('Y-m-d'))->get();
        foreach ($fairsActive as $fair){
            $fair->active = false;
            $fair->save();
        }
        $page = 'events';
        $sectors = Sector::where('active', true)->get();
        $fairs = Fair::where('active', true)->orderBy('startDate')->paginate(5);
        $sectorSelected = 0;
        $stringSelected = '';
        return view('public.main.events', compact('sectors', 'fairs', 'page', 'sectorSelected', 'stringSelected'));
    }

    public function eventsChange(Request $request)
    {
        $param = $request['param'];
        $sectorSelected = 0;
        $stringSelected = '';

        if(!is_numeric($param)){
            $fairs = Fair::where('active', true)->where('name', 'LIKE', '%'.$param.'%')->paginate(5);
            $stringSelected = $param;
        } else {
            $fairs = Fair::where('active', true)->where('sector_id', $param)->paginate(5);
            $sectorSelected = $param;
        }

        $sectors = Sector::where('active', true)->get();

        return view('public.main.components.events', compact('sectors', 'fairs', 'sectorSelected', 'stringSelected'));
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function eventsChangeGet($item)
    {

        $sectorSelected = 0;
        $stringSelected = '';
        if(!is_numeric($item)){
            $fairs = Fair::where('active', true)->where('name', 'LIKE', '%'.$item.'%')->paginate(5);
            $stringSelected = $item;
        } else {
            $fairs = Fair::where('active', true)->where('sector_id', $item)->paginate(5);
            $sectorSelected = $item;
        }

        $sectors = Sector::where('active', true)->get();
        $page = 'events';

        return view('public.main.events', compact('sectors', 'fairs', 'page', 'sectorSelected', 'stringSelected'));
    }

    public function extensions()
    {
        $page = 'extensions';
        $regions = Region::where('type', 1)->get();
        $extensions = Extension::where('active', true)->where('type', 'extension')->paginate(5);
        $images = Image::where('type', 'extensions')->get();
        return view('public.main.extensions', compact('regions', 'extensions', 'images', 'page'));
    }

    public function extensionsChange(Request $request)
    {
        $item = $request['param'];
        if(!is_numeric($item)){
            $extensions = Extension::where('active', true)->where('type', 'extension')->where('name', 'LIKE', '%'.$item.'%')->paginate(5);
        } else {
            $extensions = Extension::where('active', true)->where('type', 'extension')->where('region_id', $item)->paginate(5);
        }

        $images = Image::where('type', 'extensions')->get();

        $regions = Region::get();

        return view('public.main.components.extensions', compact('regions', 'extensions', 'images'));
    }

    public function mexico()
    {
        $page = 'mexico';
        $regions = Region::where('type', 2)->get();
        $extensions = Extension::where('active', true)->where('type', 'mexico')->paginate(5);
        $images = Image::where('type', 'mexico')->get();
        return view('public.main.mexico', compact('regions', 'extensions', 'images', 'page'));
    }

    public function mexicoChange(Request $request)
    {
        $page = 'mexico';
        $sector = $request['sector'];
        $search = $request['search'];
        if ($search != '' && $sector != null) {
            $extensions = Extension::where('active', true)
                ->where('type', 'mexico')
                ->where('destination_id', $sector)
                ->where('slug', 'LIKE', '%'.$search.'%')
                ->paginate(10);
        } else if ($search == '' && $sector == null){
            $extensions = Extension::where('active', true)
                ->where('type', 'mexico')
                ->paginate(10);
        } else if ($search != ''){
            $extensions = Extension::where('active', true)
                ->where('type', 'mexico')
                ->where('slug', 'LIKE', '%'.$search.'%')
                ->paginate(10);
        } else if($sector != null){
            $extensions = Extension::where('active', true)
                ->where('type', 'mexico')
                ->where('destination_id', $sector)
                ->paginate(10);
        }

        $regions = Region::where('type', 2)->get();
        $images = Image::where('type', 'mexico')->get();

        return view('public.main.components.mexico', compact('regions','extensions', 'images', 'page'));
    }

    public function specialTrips()
    {
        $page = 'special_trips';
        $regions = Region::where('type', 1)->get();
        $extensions = Extension::where('active', true)->where('type', 'special_trips')->paginate(5);
        $images = Image::where('type', 'special_trips')->get();
        return view('public.main.special_trips', compact('regions', 'extensions', 'images', 'page'));
    }

    public function promotion($promotion)
    {
        $page = 'promotions';
        $promotion = Promotion::findOrFail($promotion);
        $images = Image::where('type', 'promotions')->where('reference', $promotion->id)->get();
        return view('public.main.promotion', compact('promotion', 'images', 'page'));
    }

    public function specialTripsChange(Request $request)
    {
        $item = $request['param'];
        if(!is_numeric($item)){
            $extensions = Extension::where('active', true)->where('type', 'special_trips')->where('name', 'LIKE', '%'.$item.'%')->paginate(5);
        } else {
            $extensions = Extension::where('active', true)->where('type', 'special_trips')->where('region_id', $item)->paginate(5);
        }

        $regions = Region::where('type', 1)->get();
        $images = Image::where('type', 'special_trips')->get();

//        $items = ['item1', 'item2', 'item3'];

        return view('public.main.components.special_trips', compact('regions', 'extensions', 'images'));
    }

    public function news()
    {
        $page = 'news';
        $news = News::where('active', true)->where('type', 'news')->paginate(5);
        return view('public.main.news', compact('news', 'page'));
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function newsShow($slug)
    {
        $page = 'news';
        $new = News::where('active', true)->where('type', 'news')->where('slug', 'LIKE', '%'.$slug.'%')->firstOrFail();
        $description = strip_tags($new->content);
        $description = html_entity_decode($description);
        return view('public.main.new', compact('new', 'page', 'description'));
    }

    public function blogs()
    {
        $page = 'blog';
        $news = News::where('active', true)->where('type', 'blog')->orderBy('date', 'desc')->paginate(5);
        return view('public.main.news', compact('news', 'page'));
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function blog($slug)
    {
        $page = 'blog';
        $new = News::where('active', true)->where('type', 'blog')->where('slug', 'LIKE', '%'.$slug.'%')->firstOrFail();
        $description = strip_tags($new->content);
        $description = html_entity_decode($description);
        return view('public.main.new', compact('new', 'page', 'description'));
    }


    public function testimonials()
    {
        $page = 'testimonials';
        return view('public.main.testimonials', compact('page'));
    }

    public function offers()
    {
        $page = 'offers';
        $extensions = Offer::where('active', true)->paginate(5);
        $regions = Region::where('type', 2)->get();
        return view('public.main.offers', compact('extensions', 'regions', 'page'));
    }

    public function offersChange(Request $request)
    {
        $item = $request['param'];
        if(!is_numeric($item)){
            $extensions = Offer::where('active', true)->where('name', 'LIKE', '%'.$item.'%')->paginate(5);
        } else {
            $extensions = Offer::where('active', true)->where('region_id', $item)->paginate(5);
        }

        $images = Image::where('type', 'extensions')->get();

        $regions = Region::get();

        return view('public.main.components.extensions', compact('regions', 'extensions', 'images'));
    }

    public function contact()
    {
        $page = 'contact';
        return view('public.main.contact', compact('page'));
    }

    public function sendContact()
    {
        $page = 'contact';
        return view('public.main.contact', compact('page'));
    }
}
