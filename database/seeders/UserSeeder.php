<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Gerardo Trujillo',
            'email' => 'trujillo@excess.com.mx',
            'password' => Hash::make('xsadmin'),
        ]);

        User::create([
            'name' => 'Administrador',
            'email' => 'royaletrans@komextours.com',
            'password' => Hash::make('admin'),
        ]);
    }
}
