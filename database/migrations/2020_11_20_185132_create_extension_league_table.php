<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExtensionLeagueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extension_league', function (Blueprint $table) {
            $table->id();
            $table->foreignId('extension_id')
                ->references('id')
                ->on('extensions')
                ->onDelete('cascade');
            $table->foreignId('league_id')
                ->references('id')
                ->on('leagues')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extension_league');
    }
}
